#!/usr/bin/bash
# Bash Completion scriptlet for srvrepoctl
#
# Author: Daniel J. R. May <daniel.may@danieljrmay.com>
# Version: %VERSION%
#
# To test this script when you are developing it:
# > source ./srvreopctl.bash
# > srvrepoctl <TAB> <TAB>

function _srvrepoctl()
{
    # shellcheck disable=SC2034
    # words and cword are defined by _init_completion
    local cur prev words cword split
    _init_completion -s || return

    local help_opts='-h --help'
    local commands='add clean create demote destroy info list promote pull push remove repoinfo repolist update verify'

    if [[ $cword -eq 1 ]]
    then
	    mapfile -t COMPREPLY < <(compgen -W "$help_opts $commands" -- "$cur")
	    return
    fi
        
    case $prev in
	--arch)
	    local arches
	    arches=$(python -c 'from srvrepoctl.variables import Variables; print(" ".join(Variables.VALID_ARCH_VALUES))')
	    mapfile -t COMPREPLY < <(compgen -W "$arches" -- "$cur")
	    return
	    ;;
	--basearch)
	    local basearches
	    basearches=$(python -c 'from srvrepoctl.variables import Variables; print(" ".join(Variables.VALID_BASEARCH_VALUES))')
	    mapfile -t COMPREPLY < <(compgen -W "$basearches" -- "$cur")
	    return
	    ;;
	--config-dir|--gpg-homedir)
	    _filedir -d
	    return
	    ;;
        --h|--help)
	    return
	    ;;
	--keep)
	    local digits
	    digits=$(seq 0 9)
	    mapfile -t COMPREPLY < <(compgen -W "$digits" -- "$cur")
	    return
	    ;;
	--releasever)
	    local releasevers
	    releasevers=$(python3 -c 'from srvrepoctl.variables import Variables; r = range(Variables.VALID_RELEASEVER_MIN, Variables.VALID_RELEASEVER_MAX); print(" ".join([str(i) for i in r]))')
	    mapfile -t COMPREPLY < <(compgen -W "$releasevers" -- "$cur")
	    return
	    ;;
	--repo)
            local repo_ids
            repo_ids=$(_srvrepoctl_repos)
	    mapfile -t COMPREPLY < <(compgen -W "$repo_ids" -- "$cur")
	    return
	    ;;
    esac

    local assume_yes_opts='-y --assumeyes'
    local gpg_homedir_opts='--gpg-homedir'
    local keep_opts='--keep'
    local skip_update_opts='--skip-update'
    local variables_opts='--arch --basearch --releasever --repo --config-dir'
    local verbosity_opts='-d --debug -v --verbose'

    case ${words[1]} in
        --h|--help)
	    return
	    ;;
	add)
            local add_opts="$assume_yes_opts $gpg_homedir_opts $help_opts $skip_update_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$add_opts" -- "$cur")
            _filedir
	    return
	    ;;
        clean)
            local clean_opts="$assume_yes_opts $help_opts $keep_opts $skip_update_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$clean_opts" -- "$cur")
	    return
	    ;;
        create)
            local create_opts="$gpg_homedir_opts $help_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$create_opts" -- "$cur")
	    return
	    ;;
        demote)
            local demote_opts="$help_opts $gpg_homedir_opts $skip_update_opts $variables_opts $verbosity_opts"
            local artifacts
            artifacts=$(_srvrepoctl_artifact_filenames) 
	    mapfile -t COMPREPLY < <(compgen -W "$demote_opts $artifacts" -- "$cur")
	    return
	    ;;
        destroy)
            local destroy_opts="$assume_yes_opts $help_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$destroy_opts" -- "$cur")
	    return
	    ;;
        info)
            local info_opts="$gpg_homedir_opts $help_opts $variables_opts $verbosity_opts"
            local artifacts
            artifacts=$(_srvrepoctl_artifact_filenames) 
	    mapfile -t COMPREPLY < <(compgen -W "$info_opts $artifacts" -- "$cur")
	    return
	    ;;
        list)
            local list_opts="$help_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$list_opts" -- "$cur")
	    return
	    ;;
        promote)
            local promote_opts="$help_opts $gpg_homedir_opts $skip_update_opts $variables_opts $verbosity_opts"
            local artifacts
            artifacts=$(_srvrepoctl_artifact_filenames) 
	    mapfile -t COMPREPLY < <(compgen -W "$promote_opts $artifacts" -- "$cur")
	    return
	    ;;
        pull)
            local pull_opts="$help_opts $gpg_homedir_opts $skip_update_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$pull_opts" -- "$cur")
	    return
	    ;;
        push)
            local push_opts="$help_opts $gpg_homedir_opts $skip_update_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$push_opts" -- "$cur")
	    return
	    ;;
        remove)
            local remove_opts="$help_opts $skip_update_opts $variables_opts $verbosity_opts"
            local artifacts
            artifacts=$(_srvrepoctl_artifact_filenames) 
	    mapfile -t COMPREPLY < <(compgen -W "$remove_opts $artifacts" -- "$cur")
	    return
	    ;;
        repoinfo)
            local repoinfo_opts="$gpg_homedir_opts $help_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$repoinfo_opts" -- "$cur")
	    return
	    ;;
        repolist)
            local repolist_opts="$help_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$repolist_opts" -- "$cur")
	    return
	    ;;
        update)
            local update_opts="$gpg_homedir_opts $help_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$update_opts" -- "$cur")
	    return
	    ;;
        verify)
            local verify_opts="$gpg_homedir_opts $help_opts $variables_opts $verbosity_opts"
	    mapfile -t COMPREPLY < <(compgen -W "$verify_opts" -- "$cur")
	    return
	    ;;
    esac

    #echo "cur=$cur prev=$prev words=${words[*]} cword=$cword split=$split"
}
complete -F _srvrepoctl srvrepoctl

function _srvrepoctl_repos() {
    local cmd="srvrepoctl repolist "

    local -i i=2
    while [[ $i -lt ${#words[@]} ]]
    do
        if [[ "${words[$i]}" == "--arch" ]] \
               || [[ "${words[$i]}" == "--basearch" ]] \
               || [[ "${words[$i]}" == "--releasever" ]] \
               || [[ "${words[$i]}" == "--config-dir" ]]
        then
             cmd+="${words[$i]} "
             ((i++))
             cmd+="${words[$i]} "
        else
            ((i++))
        fi        
    done

    local repo_ids
    repo_ids=$(eval "$cmd")
    echo "$repo_ids"
}

function _srvrepoctl_artifact_filenames() {
    local cmd="srvrepoctl list "

    local -i i=2
    while [[ $i -lt ${#words[@]} ]]
    do
        if [[ "${words[$i]}" == "--repo" ]] \
               || [[ "${words[$i]}" == "--arch" ]] \
               || [[ "${words[$i]}" == "--basearch" ]] \
               || [[ "${words[$i]}" == "--releasever" ]] \
               || [[ "${words[$i]}" == "--config-dir" ]]
        then
             local repo_specified=true
             cmd+="${words[$i]} "
             ((i++))
             cmd+="${words[$i]} "
        else
            ((i++))
        fi        
    done
        
    local artifact_filenames
    if [[ $repo_specified = true ]]
    then
        artifact_filenames=$(eval "$cmd")
    else
        artifact_filenames=""
    fi

    echo "$artifact_filenames"
}
