<!-- markdownlint-disable MD033 -->
# <img src="doc/images/srvrepoctl-logo.png" width="100" alt="srvrepoctl logo"/> srvrepoctl

[![pipeline status](https://gitlab.com/danieljrmay/srvrepoctl/badges/master/pipeline.svg)](https://gitlab.com/danieljrmay/srvrepoctl/commits/master)

A command line utility to help with the administration of software
repositories which you serve.
