"""All configuration related code."""

import configparser
import glob
import logging
import os

from srvrepoctl import gpg
from srvrepoctl import clargs
from srvrepoctl.errors import NoConfigFilesError
from srvrepoctl.errors import NoConfigForRepoError
from srvrepoctl.errors import NonExistantConfigDirError
from srvrepoctl.errors import UnreadableConfigDirError
from srvrepoctl.repositories import Repository
from srvrepoctl.artifacts import Artifact
from srvrepoctl.variables import Variables


class SrvRepoCtl():
    """Main class for the SrvRepoCtl application.

    Configuration is represented by properties and the commands by
    methods.

    """

    def __init__(self, args_list=None, debug=False):
        """Constructor."""
        self._args = clargs.parse_args(args_list)

        if debug or self._args.debug:
            log_level = logging.DEBUG
        elif self._args.verbose:
            log_level = logging.INFO
        else:
            log_level = logging.WARNING

        logging.basicConfig(
            format="[%(levelname)s] %(message)s",
            level=log_level)

        logging.debug("Debugging mode = %s", str(debug))
        logging.debug("Command line arguments = %s", str(self._args))

        # GPG is not required for all commands we should not
        # instantiate it when it is not required.
        if hasattr(self._args, 'gpg_homedir'):
            gpg.init(self._args.gpg_homedir)
        else:
            gpg.init()

        variables = Variables(self._args.arch,
                              self._args.basearch,
                              self._args.releasever)

        self._config = configparser.ConfigParser(
            variables.dictionary,
            interpolation=configparser.ExtendedInterpolation())

        for path in self._repo_config_paths:
            logging.info("Reading repository configuration file: '%s'.", path)
            self._config.read(path)

    @property
    def artifact_list(self):
        """Get a list of artifact objects from the artifact
        filenames specified on the command line.
        """
        artifact_list = []

        for path in self.artifact_path_list:
            artifact = Artifact.new(path)
            artifact_list.append(artifact)

        return artifact_list

    @property
    def artifact_path_list(self):
        """Get a list of artifacts paths corresponding to the list of artifact
        filenames specified on the command line.
        """
        artifact_path_list = []

        if hasattr(self._args, "artifact_filename"):
            for filename in self._args.artifact_filename:
                for repo in self.repo_list:
                    artifact_path_list.append(
                        os.path.join(repo.path, filename))

        logging.debug(
            "Artifact paths: %s", ", ".join(artifact_path_list))

        return artifact_path_list

    @property
    def assume_yes(self):
        """True if we should assume "yes" to any questions."""
        return self._args.assumeyes

    @property
    def file_path_list(self):
        """Get a list of file paths specifed on the command line."""
        file_path_list = []

        if hasattr(self._args, "file_path"):
            file_path_list += self._args.file_path

        logging.debug(
            "File paths: %s", ", ".join(file_path_list))

        return file_path_list

    @property
    def keep(self):
        """Return the number of artifacts to keep when cleaning as specified by the
        command line arguments."""
        return self._args.keep

    @property
    def repo_id_list(self):
        """Return a list repo_ids.

        If specific repo-ids are specified in _args (probably via the
        command line --repo switch. We check that they are valid,
        raising a ValueError if a requested repo-id does not exist in
        the configuration files.

        If no specific repo-ids were requested in _args, we get a list
        of all repo-ids which exist in the configuration files.
        """
        if not self._args.repo:
            return self._config.sections()

        repo_id_list = []
        for repo_id in self._args.repo:
            if self.has_repo_id(repo_id):
                repo_id_list.append(repo_id)
            else:
                raise NoConfigForRepoError(repo_id)

        return repo_id_list

    @property
    def repo_list(self):
        """Return a list of repositories.

        If specific repo-ids are specified in _args (probably via the
        command line --repo switch, then those repositories are
        returned (if they exist).

        If no specific repo-ids were requested in _args, we get a list
        of all repositories which exist in the configuration files.
        """
        repo_list = []

        for repo_id in self.repo_id_list:
            repo_list.append(self.get_repo(repo_id))

        return repo_list

    @property
    def repo_list_as_str(self):
        """Return `repo_list` formatted as a string."""
        return ", ".join(["'{}'".format(r.repo_id) for r in self.repo_list])

    @property
    def skip_update(self):
        """True if we should skip repository updates."""
        return self._args.skip_update

    @property
    def verbose(self):
        """True if verbose messaging should be used."""
        return self._args.verbose

    @property
    def _repo_config_paths(self):
        """Return list of readable *.srvrepoctl configuration file paths."""
        config_dir = self._args.config_dir

        if not os.path.isdir(config_dir):
            raise NonExistantConfigDirError(config_dir)

        if not os.access(config_dir, os.R_OK):
            raise UnreadableConfigDirError(config_dir)

        paths = glob.glob(config_dir + "/*.srvrepoctl")
        readable_paths = []
        for path in paths:
            if os.access(path, os.R_OK):
                readable_paths.append(path)
            else:
                logging.warning(
                    "The repository configuration file '%s' "
                    "is not readable.", path)

        if not readable_paths:
            raise NoConfigFilesError(config_dir)

        logging.debug(
            "Found the following *.srvrepoctl paths: %s",
            str(readable_paths))
        return readable_paths

    def add(self):
        """Add artifacts to repositories."""
        for repo in self.repo_list:
            if repo.exists:
                repo.add(self.artifact_list, self.skip_update)
            else:
                question = (
                    "Repository {} at {} does not exist. "
                    "Do you want to create it (y/N)?"
                ).format(repo.repo_id, repo.path)

                user_confirms_create_and_add = self._user_confirms(
                    question,
                    ["y", "Y", "yes", "YES"])

                if user_confirms_create_and_add:
                    repo.add(self.artifact_list, self.skip_update)
                else:
                    logging.info(
                        "Skipping add to non-existant repository '%s' "
                        "because user declined to create it.",
                        repo.repo_id)

    def clean(self):
        """Clean old artifacts from repositories."""
        for repo in self.repo_list:
            repo.clean(self.skip_update, self.keep)

    def create(self):
        """Create the repositories."""
        for repo in self.repo_list:
            repo.create()

    def demote(self):
        """Demote artifacts from repositories."""
        for repo in self.repo_list:
            repo.demote(self.artifact_list, self.skip_update)

    def destroy(self):
        """Destroy the repositories."""
        for repo in self.repo_list:
            if self._user_confirms(
                    "Are you sure you want to "
                    "destroy {} (y/N)? ".format(repo.repo_id),
                    ['y', 'Y', 'yes', 'YES']):
                repo.destroy()

    def execute(self):
        """Execute the command."""
        if self._args.command == "add":
            self.add()
        elif self._args.command == "clean":
            self.clean()
        elif self._args.command == "create":
            self.create()
        elif self._args.command == "demote":
            self.demote()
        elif self._args.command == "destroy":
            self.destroy()
        elif self._args.command == "fileinfo":
            self.fileinfo()
        elif self._args.command == "info":
            self.info()
        elif self._args.command == "list":
            self.list_artifacts()
        elif self._args.command == "promote":
            self.promote()
        elif self._args.command == "pull":
            self.pull()
        elif self._args.command == "push":
            self.push()
        elif self._args.command == "remove":
            self.remove()
        elif self._args.command == "repoinfo":
            self.repoinfo()
        elif self._args.command == "repolist":
            self.repolist()
        elif self._args.command == "update":
            self.update()
        else:
            raise AssertionError(
                "Illegal value for command='{}'.".format(self._args.command))

    def fileinfo(self):
        """Print information about a specified file."""
        for path in self.file_path_list:
            Artifact.fileinfo(path)

    def get_repo(self, repo_id):
        """Return the specified repository."""
        if not self.has_repo_id(repo_id):
            raise NoConfigForRepoError(repo_id)

        return Repository.new(repo_id, self._config)

    def has_repo_id(self, repo_id):
        """Return True if the specifed repo_id exists in the current
        configuration, False otherwise."""
        return self._config.has_section(repo_id)

    def info(self):
        """Print information about a specified artifact."""
        for artifact in self.artifact_list:
            artifact.info()

    def list_artifacts(self):
        """List artifacts in the specifed repositories."""
        if not self.repo_list:
            logging.warning("Unable to list artifacts as no repositories "
                            "have been specified.")
            return

        logging.debug("Listing artifacts in %s:", self.repo_list_as_str)
        for repo in self.repo_list:
            logging.debug("List of artifacts in %s:", repo.repo_id)
            for artifact in repo.list_artifacts():
                print(artifact.filename)

    def promote(self):
        """Promote specified artifacts from specifed repositories."""
        for repo in self.repo_list:
            repo.promote(self.artifact_list, self.skip_update)

    def pull(self):
        """Pull artifacts from upstream to specifed repositories."""
        for repo in self.repo_list:
            repo.pull(self.skip_update)

    def push(self):
        """Push artifacts from specifed repositories downstream."""
        for repo in self.repo_list:
            repo.push(self.skip_update)

    def remove(self):
        """Remove specified artifacts from specifed repositories."""
        for repo in self.repo_list:
            repo.remove(self.artifact_list, self.skip_update)

    def repoinfo(self):
        """Print infomation about the specified repositorys."""
        for repo in self.repo_list:
            print(repo)

    def repolist(self):
        """Print list of available repository IDs."""
        for repo_id in self.repo_id_list:
            print(repo_id)

    def update(self):
        """Update specifed repositories."""
        for repo in self.repo_list:
            repo.update()

    def update_variables(self, variables):
        """Update the variables used by this configuration."""
        for key, value in variables.dictionary.items():
            self._config.set("DEFAULT", key, value)

    def _user_confirms(self, question, truthy_responses):
        """Get confirmation from the user."""
        if self.assume_yes:
            return True

        answer = input(question)

        if answer in truthy_responses:
            return True

        return False
