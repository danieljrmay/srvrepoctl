"""Repository configuration and actions."""

import filecmp
import logging
import glob
import os
import shutil
import subprocess
from abc import ABC, abstractmethod
from srvrepoctl import gpg
from srvrepoctl.errors import RepositoryConfigurationError, \
    UnknownRepositoryType
from srvrepoctl.artifacts import Rpm, Tarball


class Repository(ABC):
    """An abstract base class for the various repository sub-classes."""

    @staticmethod
    def new(repo_id, config):
        """Build a repository object."""
        repo_type = config.get(repo_id, "type", fallback="")

        if repo_type == "rpm":
            return RpmRepository(repo_id, config)

        if repo_type == "tarball":
            return TarballRepository(repo_id, config)

        raise UnknownRepositoryType(repo_id, repo_type)

    @abstractmethod
    def __init__(self, repo_id, config):
        """Super constructor, which must be called by concrete sub-classes."""
        self._repo_id = repo_id
        self._config = config

    @property
    def config(self):
        """Get the repository's config object."""
        return self._config

    @property
    def content(self):
        """Return repository content tags e.g. "source", etc."""
        return self.config.get(self.repo_id, "content", fallback=None)

    @property
    def demote_repo_ids(self):
        """Return a list of repository Ids.

        When demoting artifacts from this repository they are moved into
        the repositories represented by the Ids in the returned list.
        """
        return self.config.get(self.repo_id, "demote_to", fallback="").split()

    @property
    def downstream_urls(self):
        """Return a list of downstream_urls."""
        return self.config.get(
            self.repo_id, "downstream_urls", fallback="").split()

    @property
    def enabled(self):
        """Return True if repository is enabled."""
        return self.config.getboolean(self.repo_id, "enabled")

    @property
    def exists(self):
        """Return True if repository exists on disk

        This must be implemented by the specialised sub-classes.
        """
        raise NotImplementedError()

    @property
    def gpgkey(self):
        """Return absolute path to GPG public key for this repository."""
        return self.config.get(self.repo_id, "gpgkey", fallback=None)

    @property
    def keep(self):
        """Return number of old artifacts to keep when cleaning."""
        return self.config.getint(self.repo_id, "keep", fallback=1)

    @property
    def name(self):
        """Return the full repository name."""
        return self.config.get(self.repo_id, "name")

    @property
    def path(self):
        """Return the repository absolute path."""
        return self.config.get(self.repo_id, "path")

    @property
    def promote_repo_ids(self):
        """Return a list of repository Ids.

        When promoting artifacts from this repository they are moved into
        the repositories represented by the Ids in the returned list.
        """
        return self.config.get(
            self.repo_id, "promote_to", fallback="").split()

    @property
    def repo_id(self):
        """Return the repository Id."""
        return self._repo_id

    @property
    def tags(self):
        """Return tags which describe the repository e.g. "stable"."""
        return self.config.get(self.repo_id, "tags", fallback=None)

    @property
    def type(self):
        """Return the type of repository, e.g. 'rpm', 'tarball', etc."""
        return self.config.get(self.repo_id, "type", fallback="rpm")

    @property
    def upstream_urls(self):
        """Return a list of upstream_urls."""
        return self.config.get(
            self.repo_id, "upstream_urls", fallback="").split()

    def _create_directory(self):
        """Create this repositorie's directory if it does not already exist."""
        if not os.path.isdir(self.path):
            logging.debug(
                "Making directory '%s' for "
                "repository '%s'.", self.path, self.repo_id)
            os.makedirs(self.path)

        if not os.access(self.path, os.W_OK):
            raise PermissionError(
                "Repository {} does not have write access to its "
                "directory {}".format(self.repo_id, self.path))

    def contains(self, artifacts):
        """True if this repository contains all the specifed artifacts."""
        repo_dir = os.path.abspath(self.path)
        for artifact in artifacts:
            repo_artifact_path = os.path.join(repo_dir, artifact.filename)

            if not os.path.isfile(repo_artifact_path):
                return False

            if not filecmp.cmp(artifact.path,
                               repo_artifact_path, shallow=False):
                return False

        return True

    def destroy(self):
        """Destroy this repository on disk."""
        if self.exists:
            shutil.rmtree(self.path)
        else:
            logging.info(
                "Skipping destruction of '%s', because it does "
                "not exist.", self.repo_id)

    def remove(self, artifacts, skip_update=False):
        """Remove the list of artifacts from this repository."""
        for artifact in artifacts:
            if self.contains(artifact):
                artifact.delete()
            else:
                logging.info(
                    "Skipping removal of '%s', because it is not in '%s'.",
                    artifact.filename, self.repo_id)

        self.update(skip_update)

    @abstractmethod
    def add(self, artifacts, skip_update=False, passphrase=None):
        """Add artifact to this repository

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def clean(self, skip_update=False, old_artifacts_to_keep=None):
        """Clean old artifacts from this repository

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def create(self):
        """Create this repository on disk

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def demote(self, artifacts, skip_update=False):
        """Demote a list of artifacts from this repository

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def is_compatible(self, artifact):
        """True if specifed artifact is compatible with this repository

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def list_artifacts(self):
        """Return this repository's artifacts as a list of filenames

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def promote(self, artifacts, skip_update=False):
        """Promote a list of artifacts from this repository

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def pull(self, skip_update=False):
        """Pull artifacts from upstream repositories to this repository

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def push(self, skip_update=False):
        """Push artifacts from this repository to downstream repositories

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()

    @abstractmethod
    def update(self, skip_update=False):
        """Update this repository on disk

        This must be implemented by the concrete sub-class.
        """
        raise NotImplementedError()


class RpmRepository(Repository):
    """A representation of a repository."""

    def __init__(self, repo_id, config):
        """Construct a RPM repository."""
        logging.debug("Constructing RPM repository '%s'.", repo_id)
        super().__init__(repo_id, config)

        if self.type != "rpm":
            raise ValueError("Attempted to construct an RPM repository "
                             "from a configuration which "
                             "has type = '{}'.".format(self.type))

        if self.gpgkey is None and self.gpgcheck:
            raise ValueError(
                "gpgcheck is enabled for {} "
                "but no gpgkey is defined.".format(repo_id))

        if self.gpgkey is None and self.repo_gpgcheck:
            raise ValueError(
                "repo_gpgcheck is enabled for {} "
                "but no gpgkey is defined.".format(repo_id))

    @property
    def dist(self):
        """Return the repository dist e.g. ".fc29", ".el7", etc."""
        return self.config.get(self.repo_id, "dist", fallback=None)

    @property
    def distro(self):
        """Return the distro tag e.g. "cpe:/o:fedoraproject:fedora:29"."""
        return self.config.get(self.repo_id, "distro", fallback=None)

    @property
    def exists(self):
        """Return True if repository exists on disk."""
        return os.path.exists(self.path + "/repodata/repomd.xml")

    @property
    def gpgcheck(self):
        """Return True if repository artifacts should be signed and checked."""
        return self.config.getboolean(self.repo_id, "gpgcheck", fallback=True)

    @property
    def repo_gpgcheck(self):
        """Return True if repository metadata file should be signed."""
        return self.config.getboolean(
            self.repo_id, "repo_gpgcheck", fallback=False)

    def can_contain_binary_rpm(self, arch):
        """Return True if repository can contain binary RPMs of specified arch.

        If no content tag is specified or it contains any of "source",
        "src", "srpm" or "any" then we return True. All comparisons
        are case-insensitive.
        """
        if self.content is None:
            return True

        lower_case_content = self.content.lower()
        if (
                arch == "noarch" and
                (
                    "binary" in lower_case_content or
                    "bin" in lower_case_content or
                    "rpm" in lower_case_content or
                    "any" in lower_case_content
                )
        ):
            return True

        if (
                arch in lower_case_content or
                "any" in lower_case_content
        ):
            return True

        return False

    def can_contain_source_rpm(self):
        """Return True if repository can contain source RPMs.

        If no content tag is specified or it contains any of "source",
        "src", "srpm" or "any" then we return True. All comparisons
        are case-insensitive.
        """
        if self.content is None:
            return True

        lower_case_content = self.content.lower()
        if (
                "source" in lower_case_content or
                "src" in lower_case_content or
                "srpm" in lower_case_content or
                "any" in lower_case_content
        ):
            return True

        return False

    def add(self, artifacts, skip_update=False, passphrase=None):
        """Add specified RPMs to this repository."""
        if not self.exists:
            logging.info(
                "Must create repository '%s' before adding RPMs to it.",
                self.repo_id)
            self.create()

        update_required = False

        for rpm in artifacts:
            if self.is_compatible(rpm):
                shutil.copy(rpm.path, self.path)
                update_required = True
            else:
                logging.warning(
                    "Skipping add of '%s' to '%s' because it "
                    "is not compatible with the repository.",
                    rpm.filename, self.repo_id)

        if update_required:
            self.update(skip_update)
        else:
            logging.info(
                "Skipping update of '%s' because it has not been changed.",
                self.repo_id)

    def clean(self, skip_update=False, old_artifacts_to_keep=None):
        """Clean old RPMs from this repository.

        If the repository does not exist on disk then nothing is done.

        The repository is automatically updated on disk, unless
        skip_update=True.

        If specifed the old_rpms_to_keep argument overrides the value
        provided by the repositories configuration file.

        """
        if not self.exists:
            logging.info(
                "Skipping cleaning of '%s' because it has "
                "not been created.", self.repo_id)
            return

        old_rpms_paths = self.old_rpms_paths(old_artifacts_to_keep)

        for rpm_path in old_rpms_paths:
            logging.debug("Cleaning '%s' from '%s'.", rpm_path, self.repo_id)
            os.remove(rpm_path)

        logging.info(
            "Cleaned %s old RPMs "
            "from '%s'.", len(old_rpms_paths), self.repo_id)

        if not old_rpms_paths:
            logging.info("Skipping unrequired update of '%s'.", self.repo_id)
            return

        self.update(skip_update)

    def create(self):
        """Create this repository on disk.

        If the repository already exists on disk nothing is done.
        """
        if self.exists:
            logging.info(
                "Skipping creation of '%s', because it has "
                "already been created.", self.repo_id)
            return

        self._create_directory()
        cmd = ["createrepo_c", "--quiet"]

        if self.distro:
            cmd.append("--distro")
            cmd.append(self.distro)

        if self.content:
            cmd.append("--content")
            cmd.append(self.content)

        if self.tags:
            cmd.append("--repo")
            cmd.append(self.tags)

        cmd.append(self.path)
        logging.info("Creating repository '%s'.", self.repo_id)
        subprocess.run(cmd, check=True)

    def demote(self, artifacts, skip_update=False):
        """Demote a list of RPMs from this repository."""
        if not self.contains(artifacts):
            raise ValueError(
                "Not all of the specified RPMs "
                "exist in {}".format(self.repo_id))

        for demote_repo_id in self.demote_repo_ids:
            demote_repo = Repository.new(demote_repo_id, self.config)
            demote_repo.add(artifacts, skip_update)

        self.remove(artifacts, skip_update)

    def is_compatible(self, artifact):
        """Return True if specifed RPM is compatible with this repository."""
        if not isinstance(artifact, Rpm):
            logging.warning("Artifact '%s' incompatible with repository "
                            "because it is not an RPM.", artifact.filename)
            return False

        if artifact.dist != self.dist:
            logging.warning("Artifact '%s' incompatible with repository "
                            "because it’s dist value '%s' does not match "
                            "repositories.", artifact.filename, artifact.dist)
            return False

        if artifact.is_source and self.can_contain_source_rpm():
            logging.debug("Source RPM '%s' is compatible with repository.",
                          artifact.filename)
            return True

        if not artifact.is_source \
           and self.can_contain_binary_rpm(artifact.arch):
            logging.debug("Binary RPM '%s' is compatible with repository.",
                          artifact.filename)
            return True

        logging.warning("Artifact '%s' incompatible with repository.",
                        artifact.filename)
        return False

    def list_artifacts(self):
        """Return this repositorie's RPMs as a list of Rpm artifact objects."""
        if not self.exists:
            logging.info(
                "Repository '%s' has no RPMs because it has not been created.",
                self.repo_id)
            return []

        return [Rpm(path) for path in glob.glob(self.path + "/*.rpm")]

    def old_rpms_paths(self, old_rpms_to_keep=None):
        """Return a list of old RPMs absolute paths."""
        if not self.exists:
            logging.info(
                "Repository '%s' has no old RPMs because it has "
                "not been created.", self.repo_id)
            return []

        if not glob.glob(self.path + "/*.rpm"):
            logging.info(
                "Repository '%s' contains no RPMs.", self.repo_id)
            return []

        if old_rpms_to_keep is None:
            keep = self.keep
        else:
            keep = int(old_rpms_to_keep)

        cmd = ["repomanage", "--quiet", "--old",
               "--keep", str(keep),
               self.path]
        completed_process = subprocess.run(
            cmd, check=True, capture_output=True, encoding="utf-8")

        return completed_process.stdout.splitlines()

    def promote(self, artifacts, skip_update=False):
        """Promote a list of RPMs from this repository."""
        if not self.contains(artifacts):
            raise ValueError(
                "Not all of the specified RPMs "
                "exist in {}".format(self.repo_id))

        for promote_repo_id in self.promote_repo_ids:
            promote_repo = Repository.new(promote_repo_id, self.config)
            promote_repo.add(artifacts, skip_update)

        self.remove(artifacts, skip_update)

    def pull(self, skip_update=False):
        """Pull RPMs from all upstream repositories to this repository."""
        for upstream in self.upstream_urls:
            upstream_src = upstream + "/"
            cmd = ["rsync", "--archive", "--include='*.rpm'",
                   upstream_src, self.path]
            logging.info(
                "Pulling RPMs from '%s' into '%s'.", upstream, self.repo_id)
            logging.debug("Running command: %s", cmd)
            subprocess.run(cmd, check=True)

        self.update(skip_update)

    def push(self, skip_update=False):
        """Push RPMs from this repository to all downstream repositories."""
        for downstream in self.downstream_urls:
            cmd = ["rsync", "--archive"]
            rpm_paths = glob.glob(self.path + "/*.rpm")
            cmd.extend(rpm_paths)
            cmd.append(downstream + "/")
            logging.info(
                "Pushing RPMs from '%s' to '%s'.", self.repo_id, downstream)
            logging.debug("Running command: %s", cmd)
            subprocess.run(cmd, check=True)

            if not skip_update:
                # pylint: disable=W0511
                # TODO implement remote update
                logging.error("Remote update has not been implemented.")

    def remove(self, artifacts, skip_update=False):
        """Remove the list of RPMs from this repository."""
        if not self.contains(artifacts):
            raise ValueError(
                "Some of the specified RPMs are not "
                "in {}.".format(self.repo_id))

        for rpm in artifacts:
            logging.info("Removing '%s' from '%s'.", rpm.path, self.repo_id)
            os.remove(rpm.path)

        self.update(skip_update)

    def update(self, skip_update=False):
        """Update this repository on disk.

        If the repository does not exist it is created.
        """
        if skip_update:
            logging.warning(
                "Skipping update of '%s', repository "
                "metadata is now out-of-date with RPMs in "
                "repository directory.", self.repo_id)
            return

        if not self.exists:
            self.create()
            return

        cmd = ["createrepo_c", "--update", "--quiet"]

        if self.distro:
            cmd.append("--distro")
            cmd.append(self.distro)

        if self.content:
            cmd.append("--content")
            cmd.append(self.content)

        if self.tags:
            cmd.append("--repo")
            cmd.append(self.tags)

        cmd.append(self.path)
        logging.info("Updating repository '%s'.", self.repo_id)
        subprocess.run(cmd, check=True)

    def __str__(self):
        """Return an information about the current repository.

        This magic method controls the output of str(rpm) and
        print(rpm).
        """
        return ("Id:\t\t" + self.repo_id +
                "\nName:\t\t" + self.name +
                "\nPath:\t\t" + self.path +
                "\nType:\t\t" + self.type +
                "\nContent:\t" + self.content +
                "\nDist:\t\t" + self.dist +
                "\nDistro:\t\t" + str(self.distro) +
                "\nEnabled:\t" + str(self.enabled) +
                "\nExists:\t\t" + str(self.exists) +
                "\nGPG check:\t" + str(self.gpgcheck) +
                "\nGPG key:\t" + self.gpgkey +
                "\nRepo GPG check:\t" + str(self.repo_gpgcheck) +
                "\nKeep:\t\t" + str(self.keep) +
                "\nTags:\t\t" + self.tags +
                "\nDemote to:\t" + ", ".join(self.demote_repo_ids) +
                "\nPromote to:\t" + ", ".join(self.promote_repo_ids) +
                "\nDownstream:\t" + ", ".join(self.downstream_urls) +
                "\nUpstream:\t" + ", ".join(self.upstream_urls) +
                "\n")


class TarballRepository(Repository):
    """A representation of a tarball repository."""

    def __init__(self, repo_id, config):
        """Construct a tarball repository."""
        logging.debug("Constructing signed tarball repository '%s'.", repo_id)
        super().__init__(repo_id, config)

        if self.type != "tarball":
            raise ValueError("Attempted to construct a tarball repository "
                             "from a configuration which "
                             "has type = '{}'.".format(self.type))

        if self.gpgkey is None and self.gpgsign:
            raise RepositoryConfigurationError(
                "gpgsign is enabled for '{}' "
                "but no gpgkey is defined.".format(repo_id))

        if self.gpgkey is None:
            self._signing_key_id = None
        else:
            keys = gpg.scan_keys(self.gpgkey)
            self._signing_key_id = keys[0]["keyid"]

    @property
    def exists(self):
        """Return True if repository exists on disk."""
        return os.path.exists(self.path)

    @property
    def gpgsign(self):
        """Return True if repository tarballs are to be signed when added."""
        return self.config.getboolean(self.repo_id, "gpgsign", fallback=False)

    @property
    def signing_key_id(self):
        """Return the GPG signing key Id."""
        return self._signing_key_id

    def add(self, artifacts, skip_update=False, passphrase=None):
        """Add tarballs to this repository."""
        if not self.exists:
            logging.info(
                "Must create '%s' repository before adding "
                "tarballs to it.", self.repo_id)
            self.create()

        for tarball in artifacts:
            if self.is_compatible(tarball):
                repo_tarball = tarball.copy(self.path)

                if self.gpgsign:
                    gpg.sign_file(repo_tarball.path,
                                  self.signing_key_id,
                                  passphrase)
            else:
                logging.info(
                    "Skipping add of '%s' to '%s' because it "
                    "is not compatible with the repository.",
                    tarball.filename, self.repo_id)

    def clean(self, skip_update=False, old_artifacts_to_keep=None):
        """Clean old artifacts from this repository."""
        raise NotImplementedError()

    def create(self):
        """Create this repository on disk.

        If the repository already exists on disk nothing is done.
        """
        if self.exists:
            logging.info(
                "Skipping creation of '%s', because it has "
                "already been created.", self.repo_id)
            return

        self._create_directory()

    def demote(self, artifacts, skip_update=False):
        """Demote a list of artifacts from this repository."""
        raise NotImplementedError()

    def is_compatible(self, artifact):
        """True if specifed artifact is compatible with this repository."""
        if isinstance(artifact, Tarball):
            logging.debug("Tarball artifact '%s' is compatible with "
                          "repository.", artifact.filename)
            return True

        logging.warning("Non-tarball artifact '%s' is incompatible with "
                        "repository.", artifact.filename)
        return False

    def list_artifacts(self):
        """Return this repository's artifacts as a list of filenames."""
        if not self.exists:
            logging.info(
                "Repository '%s' has no tarballs because "
                "it has not been created.", self.repo_id)
            return []

        return [Tarball(dir_entry.path)
                for dir_entry in os.scandir(self.path)
                if Tarball.is_valid_path(dir_entry.path)]

    def promote(self, artifacts, skip_update=False):
        """Promote a list of artifacts from this repository."""
        raise NotImplementedError()

    def pull(self, skip_update=False):
        """Pull artifacts from upstream repositories to this repository."""
        raise NotImplementedError()

    def push(self, skip_update=False):
        """Push artifacts from this repository to downstream repositories."""
        for downstream in self.downstream_urls:
            cmd = ["rsync", "--archive"]
            paths = list(
                map(lambda sig_or_tarball_entry: sig_or_tarball_entry.path,
                    filter(lambda entry: entry.is_file()
                           and (entry.name.endswith(".asc")
                                or Tarball.is_valid_filename(entry.name)),
                           os.scandir(self.path))))

            cmd.extend(paths)
            cmd.append(downstream + "/")
            logging.info(
                "Pushing RPMs from '%s' to '%s'.", self.repo_id, downstream)
            logging.debug("Running command: %s", cmd)
            subprocess.run(cmd, check=True)

            if not skip_update:
                # pylint: disable=W0511
                # TODO implement remote update
                logging.warning("Remote update has not been implemented.")

    def update(self, skip_update=False):
        """Update this repository on disk."""
        raise NotImplementedError()

    def __str__(self):
        """Return an information about the current repository.

        This magic method controls the output of str(tarball_repository) and
        print(tarball_repository).
        """
        return ("Id:\t\t" + self.repo_id +
                "\nName:\t\t" + self.name +
                "\nType:\t\t" + self.type +
                "\nPath:\t\t" + self.path +
                "\nContent:\t" + self.content +
                "\nEnabled:\t" + str(self.enabled) +
                "\nExists:\t\t" + str(self.exists) +
                "\nGPG check:\t" + str(self.gpgsign) +
                "\nGPG key:\t" + self.gpgkey +
                "\nKeep:\t\t" + str(self.keep) +
                "\nTags:\t\t" + self.tags +
                "\nDemote to:\t" + ", ".join(self.demote_repo_ids) +
                "\nPromote to:\t" + ", ".join(self.promote_repo_ids) +
                "\nDownstream:\t" + ", ".join(self.downstream_urls) +
                "\nUpstream:\t" + ", ".join(self.upstream_urls) +
                "\n")
