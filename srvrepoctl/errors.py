"""Errors and exit codes from srvrepoctl."""


class SrvRepoCtlError(Exception):
    """Base class for all srvrepoctl exceptions."""

    EXIT_OK = 0
    EXIT_VALUE_ERROR = 1
    EXIT_NON_EXISTANT_CONFIG_DIR = 2
    EXIT_PERMISSION_ERROR = 3
    EXIT_FILE_NOT_FOUND_ERROR = 4
    EXIT_CONFIGURATION_ERROR = 5
    EXIT_UNREADABLE_CONFIG_DIR = 6
    EXIT_NO_CONFIG_FILES = 7
    EXIT_UNKNOWN_ARTIFACT = 8

    def __init__(self, message, exit_status):
        super().__init__(message)
        self.exit_status = exit_status


class NoConfigForRepoError(SrvRepoCtlError):
    """No configuration exists for a repository ID."""

    def __init__(self, repo_id):
        super().__init__(
            "No configuration exists for the "
            "repository ID '{}'.".format(repo_id),
            self.EXIT_CONFIGURATION_ERROR)


class NonExistantConfigDirError(SrvRepoCtlError):
    """The specified configuration directory does not exist."""

    def __init__(self, config_dir):
        super().__init__(
            "The specified config_dir='{}' "
            "is not a directory.".format(config_dir),
            self.EXIT_NON_EXISTANT_CONFIG_DIR)


class UnreadableConfigDirError(SrvRepoCtlError):
    """The specified configuration directory does not exist."""

    def __init__(self, config_dir):
        super().__init__(
            "The specified config_dir='{}' "
            "is not readable.".format(config_dir),
            self.EXIT_UNREADABLE_CONFIG_DIR)


class NoConfigFilesError(SrvRepoCtlError):
    """No configuration files."""

    def __init__(self, config_dir):
        super().__init__(
            "No readable *.srvrepoctl repository configuration "
            "files were found in the '{}' directory.".format(config_dir),
            self.EXIT_NO_CONFIG_FILES)


class UnknownRepositoryType(SrvRepoCtlError):
    """The unknown type parameter value in repository configuration file."""

    def __init__(self, repo_id, repo_type):
        super().__init__(
            "Unkown repository type of '{}' for "
            "repository ID '{}'.".format(repo_type, repo_id),
            self.EXIT_CONFIGURATION_ERROR)


class RepositoryConfigurationError(SrvRepoCtlError):
    """There is some error in the repository configuration."""

    def __init__(self, msg):
        super().__init__(msg, self.EXIT_CONFIGURATION_ERROR)


class UnknownArtifactError(SrvRepoCtlError):
    """Error corresponding to an unknown artifact."""

    def __init__(self, path):
        super().__init__(
            "The path '{}' does not correspond to a known "
            "artifact file-type.".format(path),
            self.EXIT_UNKNOWN_ARTIFACT)
