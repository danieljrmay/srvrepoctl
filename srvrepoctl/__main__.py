#!/usr/bin/env python3

"""Command line interface for the srvrepoctl utility."""

import logging
import sys

from srvrepoctl.errors import SrvRepoCtlError
from srvrepoctl.srvrepoctl import SrvRepoCtl


def main():
    """Entry point for srvrepoctl command line application."""
    try:
        srvrepoctl = SrvRepoCtl(args_list=None, debug=False)
        srvrepoctl.execute()
        return 0
    except (SrvRepoCtlError) as srvrepoctl_error:
        logging.error(str(srvrepoctl_error))
        return srvrepoctl_error.exit_status


if __name__ == '__main__':
    sys.exit(main())
