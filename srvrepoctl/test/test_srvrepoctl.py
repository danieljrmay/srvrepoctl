#!/usr/bin/env python3

"""Tests the srvrepoctl.py module."""

import logging
import random
import unittest
from srvrepoctl.srvrepoctl import SrvRepoCtl
from srvrepoctl.test.utils import get_random_string
from srvrepoctl.test import constants
from srvrepoctl.variables import Variables


class TestSrvRepoCtl(unittest.TestCase):
    """Test the SrvRepoCtl class."""

    def setUp(self):
        """Construct objects used for testing."""
        self.basearch = random.choice(Variables.VALID_BASEARCH_VALUES)
        self.releasever = random.randint(
            Variables.VALID_RELEASEVER_MIN,
            Variables.VALID_RELEASEVER_MAX)

        logging.basicConfig(level=logging.DEBUG,
                            filename="test.log",
                            filemode="w")

        clargs = [
            'repoinfo',
            '--config-dir', constants.REPOS_DIR,
            '--basearch', self.basearch,
            '--releasever', str(self.releasever),
            ]

        self.srvrepoctl = SrvRepoCtl(clargs)

    def test_repo_id_list(self):
        """Test the repo_id_list property."""
        expected_repo_id_list = ["rpm-repo",
                                 "rpm-repo-source",
                                 "rpm-repo-testing",
                                 "rpm-repo-testing-source",
                                 "tarball-repo",
                                 "tarball-repo-testing"]

        for repo_id in expected_repo_id_list:
            self.assertIn(repo_id, self.srvrepoctl.repo_id_list)

    def test_has_repo_id(self):
        """Test the has_repo_id(repo_id) method."""
        for repo_id in self.srvrepoctl.repo_id_list:
            self.assertTrue(self.srvrepoctl.has_repo_id(repo_id))

        for i in range(10):
            random_str = get_random_string(i)
            if random_str not in self.srvrepoctl.repo_id_list:
                self.assertFalse(self.srvrepoctl.has_repo_id(random_str))


if __name__ == '__main__':
    unittest.main()
