#!/usr/bin/env python3

"""Tests the srvrepoctl.py module."""

import platform
import unittest

import distro

from srvrepoctl.variables import Variables
from srvrepoctl.test.utils import get_random_string


class TestVariables(unittest.TestCase):
    """Test the Variables class."""

    def setUp(self):
        """Construct a Variables object to test."""
        self.variables = Variables()

    def test_default_values(self):
        """Test the default values provided by dnf."""
        defaults = self.variables.dictionary

        self.assertEqual(defaults["arch"], platform.processor())
        self.assertEqual(defaults["basearch"], platform.machine())
        self.assertEqual(defaults["releasever"], distro.version())

    def test_arch(self):
        """Test the arch property."""
        for arch in Variables.VALID_ARCH_VALUES:
            if arch != "noarch":
                self.variables.override_arch(arch)
                self.assertEqual(self.variables.arch, arch)
                self.assertEqual(self.variables.dictionary["arch"], arch)

        for i in range(10):
            rand_str = get_random_string(i)
            if rand_str in Variables.VALID_ARCH_VALUES:
                continue

            with self.assertRaises(ValueError):
                self.variables.override_arch(rand_str)

    def test_basearch(self):
        """Test the basearch property."""
        for basearch in Variables.VALID_BASEARCH_VALUES:
            if basearch != "noarch":
                self.variables.override_basearch(basearch)
                self.assertEqual(self.variables.basearch, basearch)
                self.assertEqual(
                    self.variables.dictionary["basearch"],
                    basearch)

        for i in range(10):
            rand_str = get_random_string(i)
            if rand_str in Variables.VALID_BASEARCH_VALUES:
                continue

            with self.assertRaises(ValueError):
                self.variables.override_basearch(rand_str)

    def test_releasever(self):
        """Test the releasever property."""
        for releasever in range(
                Variables.VALID_RELEASEVER_MIN,
                Variables.VALID_RELEASEVER_MAX + 1):
            self.variables.override_releasever(releasever)
            self.assertEqual(self.variables.releasever, str(releasever))
            self.assertEqual(
                self.variables.dictionary["releasever"], str(releasever))

        for i in range(
                Variables.VALID_RELEASEVER_MIN - 10,
                Variables.VALID_RELEASEVER_MIN - 1):
            with self.assertRaises(ValueError):
                self.variables.override_releasever(i)

        for i in range(
                Variables.VALID_RELEASEVER_MAX + 1,
                Variables.VALID_RELEASEVER_MAX + 20):
            with self.assertRaises(ValueError):
                self.variables.override_releasever(i)

        for i in range(10):
            rand_str = get_random_string(5)
            with self.assertRaises(ValueError):
                self.variables.override_releasever(rand_str)


if __name__ == '__main__':
    unittest.main()
