"""Utility methods for tests."""

import random
import string


def get_random_string(length):
    """Return a random string of the specified length."""
    return "".join(
        random.choice(string.ascii_letters + string.digits)
        for i in range(length))
