#!/usr/bin/env python3

"""Tests the srvrepoctl.py module."""

import logging
import os
import shutil
import unittest

from srvrepoctl.test import constants
from srvrepoctl import gpg

TEST_DIR = os.path.join("/tmp", __name__)


class TestGpg(unittest.TestCase):
    """ Test the gpg module."""

    @classmethod
    def setUpClass(cls):
        """Run before each test in this class."""
        os.makedirs(TEST_DIR, exist_ok=False)

    def test_is_init(self):
        """Test the is_init() method."""
        if gpg.is_init():
            gpg.uninit()
            self.assertFalse(gpg.is_init())
        else:
            gpg.init()
            self.assertTrue(gpg.is_init())

    def test_uninit(self):
        """Test the uninit() method."""
        if gpg.is_init():
            gpg.uninit()
        else:
            gpg.init()
            gpg.uninit()

        self.assertFalse(gpg.is_init())

    def test_init(self):
        """Test init() method."""
        gpg.uninit()
        with self.assertRaises(gpg.GpgUninitialisedError):
            gpg.gnupghome()

        gpg.init(TEST_DIR)
        self.assertEqual(gpg.gnupghome(), TEST_DIR)

    def test_homedir(self):
        """Test that config has run correctly in setUpModule()"""
        gpg.init(TEST_DIR)
        self.assertEqual(gpg.gnupghome(), TEST_DIR)

    def test_import_keys(self):
        """Test import_keys()."""
        gpg.init(TEST_DIR)
        result = gpg.import_keys(constants.SRVREPOCTL_SECRET_KEY)
        logging.debug(repr(result))
        self.assertEqual(result.fingerprints[0],
                         "92877E0ABAD3EF9DE512E0498DA3B789744C9D9E")

    def test_scan_keys(self):
        """Test scan_keys()."""
        gpg.init(TEST_DIR)
        result = gpg.scan_keys(constants.SRVREPOCTL_PUBLIC_KEY)
        self.assertEqual(result[0]['keyid'],
                         constants.SRVREPOCTL_PUBLIC_KEY_ID)

    def test_sign_file(self):
        """Test sign_file()."""
        gpg.init(TEST_DIR)
        gpg.import_keys(constants.SRVREPOCTL_SECRET_KEY)

        path = os.path.join(TEST_DIR, "test.txt")
        with open(path, "wt") as test_file:
            test_file.write("This is a test file.")

        gpg.sign_file(path,
                      constants.SRVREPOCTL_PUBLIC_KEY_ID,
                      constants.SRVREPOCTL_SECRET_KEY_PWD)
        signature_path = path + ".asc"
        self.assertTrue(os.path.isfile(signature_path))

    @classmethod
    def tearDownClass(cls):
        """Run after each test in this class."""
        shutil.rmtree(TEST_DIR)


if __name__ == '__main__':
    unittest.main()
