#!/usr/bin/env python3

"""Tests the artifact.py module."""

import os
import shutil
import unittest

from srvrepoctl.artifacts import Artifact, Rpm, Tarball
from srvrepoctl.errors import UnknownArtifactError
from srvrepoctl.variables import Variables
from srvrepoctl.test import constants


TEST_DIR = os.path.join("/tmp", __name__)


def setUpModule():  # pylint: disable=invalid-name
    """Run before all tests in this module."""
    os.makedirs(TEST_DIR, exist_ok=False)


class TestArtifact(unittest.TestCase):
    """Test the Artifact abstract base class."""

    def test_filename(self):
        """Test the filename property."""
        rpm_filename = "basesystem-11-6.fc29.noarch.rpm"
        rpm = Artifact.new(constants.RPM_DIR + rpm_filename)
        self.assertEqual(rpm.filename, rpm_filename)

    def test_path(self):
        """Test the path property."""
        rpm_filename = "basesystem-11-6.fc29.noarch.rpm"
        rpm_path = constants.RPM_DIR + rpm_filename
        rpm = Artifact.new(rpm_path)
        self.assertEqual(rpm.path, rpm_path)

    def test_copy(self):
        """Test the copy method."""
        src_filename = "basesystem-11-6.fc29.noarch.rpm"
        src_path = constants.RPM_DIR + src_filename
        rpm = Artifact.new(src_path)

        dst_path = TEST_DIR
        rpm_copy = rpm.copy(dst_path)

        self.assertEqual(rpm_copy.path, os.path.join(dst_path, src_filename))
        rpm_copy.delete()

    def test_delete(self):
        """Test the delete method."""
        src_filename = "basesystem-11-6.fc29.noarch.rpm"
        src_path = constants.RPM_DIR + src_filename
        rpm = Artifact.new(src_path)

        dst_path = TEST_DIR
        rpm_copy = rpm.copy(dst_path)

        self.assertEqual(rpm_copy.path, os.path.join(dst_path, src_filename))
        rpm_copy.delete()
        self.assertFalse(os.path.isfile(rpm_copy.path))


class TestRpm(unittest.TestCase):
    """Test the Rpm class."""

    def setUp(self):
        """Construct objects used for testing."""
        self.variables = Variables()

    def test_fc29_binary_x86_64_rpm(self):
        """Check the metadata of a signed, valid, binary fc29 RPM."""
        filename = "basesystem-11-6.fc29.noarch.rpm"
        path = constants.RPM_DIR + filename
        self.assertTrue(Rpm.is_valid_path(path))

        rpm = Artifact.new(path)
        self.assertEqual(rpm.filename, filename)
        self.assertFalse(rpm.is_source)
        self.assertEqual(rpm.name, "basesystem")
        self.assertEqual(rpm.version, "11")
        self.assertEqual(rpm.release, "6.fc29")
        self.assertEqual(rpm.arch, "noarch")
        self.assertEqual(rpm.specver, "6")
        self.assertEqual(rpm.dist, ".fc29")
        self.assertEqual(rpm.distro_code, "fc")
        self.assertEqual(rpm.releasever, "29")
        self.assertTrue("a20aa56b429476b4" in rpm.signature)

    def test_fc30_binary_x86_64_rpm(self):
        """Check the metadata of a signed, valid, binary fc30 RPM."""
        filename = "basesystem-11-7.fc30.noarch.rpm"
        path = constants.RPM_DIR + filename
        self.assertTrue(Rpm.is_valid_path(path))

        rpm = Artifact.new(path)
        self.assertEqual(rpm.filename, filename)
        self.assertFalse(rpm.is_source)
        self.assertEqual(rpm.name, "basesystem")
        self.assertEqual(rpm.version, "11")
        self.assertEqual(rpm.release, "7.fc30")
        self.assertEqual(rpm.arch, "noarch")
        self.assertEqual(rpm.specver, "7")
        self.assertEqual(rpm.dist, ".fc30")
        self.assertEqual(rpm.distro_code, "fc")
        self.assertEqual(rpm.releasever, "30")
        self.assertTrue("ef3c111fcfc659b9" in rpm.signature)

    def test_fc29_source_rpm(self):
        """Check the metadata of a signed, valid, source fc29 RPM."""
        filename = "basesystem-11-6.fc29.src.rpm"
        path = constants.RPM_DIR + filename
        self.assertTrue(Rpm.is_valid_path(path))

        rpm = Artifact.new(path)
        self.assertEqual(rpm.filename, filename)
        self.assertTrue(rpm.is_source)
        self.assertEqual(rpm.name, "basesystem")
        self.assertEqual(rpm.version, "11")
        self.assertEqual(rpm.release, "6.fc29")
        self.assertEqual(rpm.arch, "noarch")
        self.assertEqual(rpm.specver, "6")
        self.assertEqual(rpm.dist, ".fc29")
        self.assertEqual(rpm.distro_code, "fc")
        self.assertEqual(rpm.releasever, "29")
        self.assertTrue("a20aa56b429476b4" in rpm.signature)

    def test_fc30_source_rpm(self):
        """Check the metadata of a signed, valid, source fc30 RPM."""
        filename = "basesystem-11-7.fc30.src.rpm"
        path = constants.RPM_DIR + filename
        self.assertTrue(Rpm.is_valid_path(path))

        rpm = Artifact.new(path)
        self.assertEqual(rpm.filename, filename)
        self.assertTrue(rpm.is_source)
        self.assertEqual(rpm.name, "basesystem")
        self.assertEqual(rpm.version, "11")
        self.assertEqual(rpm.release, "7.fc30")
        self.assertEqual(rpm.arch, "noarch")
        self.assertEqual(rpm.specver, "7")
        self.assertEqual(rpm.dist, ".fc30")
        self.assertEqual(rpm.distro_code, "fc")
        self.assertEqual(rpm.releasever, "30")
        self.assertTrue("ef3c111fcfc659b9" in rpm.signature)


class TestTarball(unittest.TestCase):
    """ Test the Tarball class."""

    def test_valid_tarball_path(self):
        """Check we can create a Tarball class from valid paths."""
        for ext in ["tar", "tar.bz2", "tar.gz", "tar.xz"]:
            filename = "test." + ext
            path = constants.TARBALL_DIR + filename
            self.assertTrue(Tarball.is_valid_path(path))

            tarball = Artifact.new(path)
            self.assertEqual(tarball.path, path)
            self.assertEqual(tarball.filename, filename)

    def test_invalid_tarball_path(self):
        """Check we get error when creating a Tarball from an invalid path."""
        filename = "test.txt"
        path = constants.NON_ARTIFACT_DIR + filename
        self.assertFalse(Tarball.is_valid_path(path))

        with self.assertRaises(UnknownArtifactError):
            Artifact.new(path)


def tearDownModule():  # pylint: disable=invalid-name
    """Run after all tests in this module."""
    shutil.rmtree(TEST_DIR)


if __name__ == '__main__':
    unittest.main()
