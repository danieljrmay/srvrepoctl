#!/usr/bin/env python3

"""Tests the srvrepoctl.py module."""

import configparser
import logging
import os
import shutil
import unittest

from srvrepoctl import gpg
from srvrepoctl.artifacts import Artifact
from srvrepoctl.repositories import Repository, RpmRepository, \
    TarballRepository
from srvrepoctl.test import constants

TEST_DIR = os.path.join("/tmp", __name__)


def setUpModule():  # pylint: disable=invalid-name
    """Run before all tests in this module."""
    os.makedirs(TEST_DIR, exist_ok=False)
    gpg.init(TEST_DIR)
    gpg.import_keys(constants.SRVREPOCTL_SECRET_KEY)
    shutil.copy(constants.SRVREPOCTL_PUBLIC_KEY, TEST_DIR)


class TestRespository(unittest.TestCase):
    """ Test the Repository abstract base class."""

    def setUp(self):
        """Construct objects used for testing."""
        logging.basicConfig(level=logging.DEBUG,
                            filename="test.log",
                            filemode="w")

        variables = {'arch': 'x86-64',
                     'basearch': 'x86-64',
                     'releasever': '29'}

        rpm_repo_config_path = constants.REPOS_DIR + "rpm-repo.srvrepoctl"
        rpm_repo_config = configparser.ConfigParser(
            variables,
            interpolation=configparser.ExtendedInterpolation())
        rpm_repo_config.read(rpm_repo_config_path)
        self.rpm_repo = Repository.new("rpm-repo", rpm_repo_config)

    def test_new(self):
        """Test the new static method."""
        self.assertTrue(isinstance(self.rpm_repo, RpmRepository))

    def test_config(self):
        """Test the config property."""
        self.assertTrue(isinstance(self.rpm_repo.config,
                                   configparser.ConfigParser))

    def test_content(self):
        """Test the content property."""
        self.assertEqual(self.rpm_repo.content, "binary-x86-64")

    def test_demote_repo_ids(self):
        """Test the demote_repo_ids property."""
        self.assertEqual(self.rpm_repo.demote_repo_ids, ['rpm-repo-testing'])

    def test_downsream_urls(self):
        """Test the downstream_urls property."""
        self.assertEqual(
            self.rpm_repo.downstream_urls,
            ['root@mirror.example.org:/srv/repo/rpm/fedora/29/x86-64'])

    def test_enabled(self):
        """Test the enabled property."""
        self.assertEqual(self.rpm_repo.enabled, True)

    def test_gpgkey(self):
        """Test the gpgkey property."""
        self.assertEqual(
            self.rpm_repo.gpgkey,
            'file:///etc/pki/rpm-gpg/RPM-GPG-KEY-test')

    def test_keep(self):
        """Test the keep property."""
        self.assertEqual(self.rpm_repo.keep, 2)

    def test_name(self):
        """Test the name property."""
        self.assertEqual(self.rpm_repo.name, 'RPM Repo — Fedora 29 — x86-64')

    def test_path(self):
        """Test the path property."""
        self.assertEqual(
            self.rpm_repo.path,
            '/tmp/srvrepoctl.test.test_repositories/repo/rpm/fedora/29/x86-64')

    def test_promote_repo_ids(self):
        """Test the promote_repo_ids property."""
        self.assertEqual(self.rpm_repo.promote_repo_ids, [])

    def test_repo_id(self):
        """Test the repo_id property."""
        self.assertEqual(self.rpm_repo.repo_id, 'rpm-repo')

    def test_tags(self):
        """Test the tags property."""
        self.assertEqual(self.rpm_repo.tags, "stable")

    def test_type(self):
        """Test the type property."""
        self.assertEqual(self.rpm_repo.type, "rpm")

    def test_upsream_urls(self):
        """Test the upstream_urls property."""
        self.assertEqual(
            self.rpm_repo.upstream_urls,
            [])

    # Done up to but not including _create_directory()
    # Implement tests for: add clean contains create demote destroy
    # is_compatible list old_rpms_paths promote pull push remove
    # update _create_directory


class RpmRespository(unittest.TestCase):
    """ Test the RpmRepository class."""

    def setUp(self):
        """Construct objects used for testing."""
        logging.basicConfig(level=logging.DEBUG,
                            filename="test-rpm-repo.log",
                            filemode="w")

        variables = {'arch': 'x86-64',
                     'basearch': 'x86-64',
                     'releasever': '29'}

        rpm_repo_config_path = constants.REPOS_DIR + "rpm-repo.srvrepoctl"
        rpm_repo_config = configparser.ConfigParser(
            variables,
            interpolation=configparser.ExtendedInterpolation())
        rpm_repo_config.read(rpm_repo_config_path)
        self.rpm_repo = RpmRepository("rpm-repo", rpm_repo_config)

    def test_dist(self):
        """Test the dist property."""
        self.assertEqual(self.rpm_repo.dist, '.fc29')

    def test_distro(self):
        """Test the distro property."""
        self.assertEqual(self.rpm_repo.distro,
                         'cpe:/o:fedoraproject:fedora:29')

    def test_exists(self):
        """Test the exists property."""
        self.assertEqual(self.rpm_repo.exists, False)

    def test_gpgcheck(self):
        """Test the gpgcheck property."""
        self.assertEqual(self.rpm_repo.gpgcheck, True)

    def test_repo_gpgcheck(self):
        """Test the repo_gpgcheck property."""
        self.assertEqual(self.rpm_repo.repo_gpgcheck, False)

    def test_can_contain_binary_rpm(self):
        """Test the can_contain_binary_rpm(arch) method."""
        self.assertTrue(self.rpm_repo.can_contain_binary_rpm("x86-64"))

    def test_can_contain_source_rpm(self):
        """Test the can_contain_source_rpm(arch) method."""
        self.assertFalse(self.rpm_repo.can_contain_source_rpm())

    # Done up to but not including add()


class TarballRespository(unittest.TestCase):
    """ Test the TarballRepository class."""

    def setUp(self):
        """Run before every test in this class."""
        variables = {'arch': 'x86-64',
                     'basearch': 'x86-64',
                     'releasever': '31'}

        tarball_repo_config_path = constants.REPOS_DIR + \
            "tarball-repo.srvrepoctl"
        tarball_repo_config = configparser.ConfigParser(
            variables,
            interpolation=configparser.ExtendedInterpolation())
        tarball_repo_config.read(tarball_repo_config_path)
        self.tarball_repo = TarballRepository("tarball-repo",
                                              tarball_repo_config)

    def test_exists(self):
        """Test the exists property."""
        if self.tarball_repo.exists:
            self.assertTrue(os.path.isdir(self.tarball_repo.path))
        else:
            self.assertFalse(os.path.isdir(self.tarball_repo.path))

    def test_gpgsign(self):
        """Test the gpgsign property."""
        self.assertTrue(self.tarball_repo.gpgsign)

    def test_signing_key_id(self):
        """Test the gpgsign property."""
        self.assertEqual(self.tarball_repo.signing_key_id, "8DA3B789744C9D9E")

    def test_add(self):
        """Test add commmand."""
        tarball = Artifact.new(constants.TARBALL_DIR + "test.tar.xz")
        tarballs = [tarball]
        self.tarball_repo.add(tarballs, passphrase="srvrepoctl")
        signature_path = os.path.join(self.tarball_repo.path,
                                      "test.tar.xz.asc")
        self.assertTrue(os.path.isfile(signature_path))
        self.assertTrue(self.tarball_repo.contains(tarballs))


def tearDownModule():  # pylint: disable=invalid-name
    """Run before all tests in this module."""
    shutil.rmtree(TEST_DIR)


if __name__ == '__main__':
    unittest.main()
