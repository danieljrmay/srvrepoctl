"""Constants used throughout the test codebase."""

TEST_DIR = "srvrepoctl/test/"
KEY_DIR = TEST_DIR + "key/"
SRVREPOCTL_PUBLIC_KEY_FILENAME = "srvrepoctl-testing-key-2020.public-key.asc"
SRVREPOCTL_PUBLIC_KEY = KEY_DIR + SRVREPOCTL_PUBLIC_KEY_FILENAME
SRVREPOCTL_PUBLIC_KEY_ID = "8DA3B789744C9D9E"
SRVREPOCTL_SECRET_KEY_FILENAME = "srvrepoctl-testing-key-2020.secret-key.asc"
SRVREPOCTL_SECRET_KEY = KEY_DIR + SRVREPOCTL_SECRET_KEY_FILENAME
SRVREPOCTL_SECRET_KEY_PWD = "srvrepoctl"
NON_ARTIFACT_DIR = TEST_DIR + "non-artifact/"
REPOS_DIR = TEST_DIR + "srvrepoctl.repos.d/"
RPM_DIR = TEST_DIR + "rpm/"
RPM_GPG_KEY_DIR = TEST_DIR + "rpm-gpg/"
TARBALL_DIR = TEST_DIR + "tarball/"
