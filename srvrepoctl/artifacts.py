"""Artifact abstract base class."""

import logging
import os
import re
import shutil
import subprocess
from abc import ABC, abstractmethod
from srvrepoctl.errors import UnknownArtifactError


class Artifact(ABC):
    """An abstract base class representing objects in a repository

    Typical artifacts are RPM files or tarball files. This abstract
    base class provides basic file operations on an artifact. The
    concrete classes which extend artifact like `Rpm` and `Tarball`
    provide more specialist implementations.
    """

    @staticmethod
    def fileinfo(path):
        """Get information about a file as a potential artifact.
        """
        if Rpm.is_valid_path(path):
            rpm = Rpm(path)
            rpm.info()
        elif Tarball.is_valid_path(path):
            tarball = Tarball(path)
            tarball.info()
        else:
            print("type=Unkown")

    @staticmethod
    def new(path):
        """Build an artifact

        The artifact type is detected by the file extension of the the
        provided path.
        """
        if Rpm.is_valid_path(path):
            return Rpm(path)

        if Tarball.is_valid_path(path):
            return Tarball(path)

        raise UnknownArtifactError(path)

    @abstractmethod
    def __init__(self, path):
        """Called by concrete class constructors."""
        if os.path.isfile(path):
            self._path = path
        else:
            raise FileNotFoundError("There is no file with path: " + path)

    @property
    def filename(self):
        """Get the artifact’s filename."""
        return os.path.basename(self._path)

    @property
    def path(self):
        """Get the artifact’s path."""
        return self._path

    @property
    def size(self):
        """Get the artifact’s file size in bytes."""
        os.path.getsize(self._path)

    def copy(self, destination_path):
        """Copy the artifact file to the destination_path."""
        logging.debug("Copying '%s' to '%s'.", self._path, destination_path)
        return Artifact.new(shutil.copy(self._path, destination_path))

    def delete(self):
        """Delete this artifact from the filesystem."""
        logging.debug("Deleting '%s'.", self._path)
        os.remove(self._path)

    def detached_signature(self):
        """Get the associated detached signature if it exists."""
        detached_signature_path = self.path + ".asc"

        if DetachedSignature.is_valid_path(detached_signature_path):
            return DetachedSignature(detached_signature_path)

        return None


class Rpm(Artifact):
    """Represents a RPM file.

    There is some good documentation about RPM head tags at
    https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/ch-package-structure.html

    """

    QUERYFORMAT_REGEX = re.compile(
        r"^NAME=(.*)$\n"
        r"^VERSION=(.*)$\n"
        r"^RELEASE=(.*)$\n"
        r"^DISTRIBUTION=(.*)$\n"
        r"^ARCH=(.*)$\n"
        r"^SOURCEPACKAGE=(.*)$\n"
        r"^SIGNATURE=(.*)$\n",
        re.MULTILINE
    )

    RELEASE_REGEX = re.compile(r"^(\d+)\.*(el|fc)(\d+)(.*)$")

    SIGNATURE_REGEX = re.compile(r"^(.+),\ (.+),\ Key\ ID\ (.+)$")

    @staticmethod
    def is_valid_path(path):
        """Return true if path corresponds to a tarball."""
        return path.endswith(".rpm") and os.path.isfile(path)

    def __init__(self, path):
        """Constructor."""
        if Rpm.is_valid_path(path):
            super().__init__(path)
        else:
            raise ValueError("The path {} does not correspond to "
                             "a valid RPM file.".format(path))

        cmd = ["rpm", "--query", "--package",
               "--queryformat",
               "NAME=%{NAME}\n" +
               "VERSION=%{VERSION}\n" +
               "RELEASE=%{RELEASE}\n" +
               "DISTRIBUTION=%{DISTRIBUTION}\n" +
               "ARCH=%{ARCH}\n" +
               "SOURCEPACKAGE=%{SOURCEPACKAGE}\n" +
               "SIGNATURE=%{SIGPGP:pgpsig}\n",
               path]

        completed_process = subprocess.run(cmd, capture_output=True,
                                           check=True, encoding="utf-8")

        match = Rpm.QUERYFORMAT_REGEX.fullmatch(completed_process.stdout)
        if match:
            self.name = match.group(1)
            self.version = match.group(2)
            self.release = match.group(3)
            self.distribution = match.group(4)
            self.arch = match.group(5)
            self.is_source = bool(match.group(6) == "1")
            self.signature = match.group(7)

        release_match = Rpm.RELEASE_REGEX.fullmatch(self.release)
        if release_match:
            self.specver = release_match.group(1)
            self.distro_code = release_match.group(2)
            self.releasever = release_match.group(3)

        self.signature_format = None
        self.signature_date = None
        self.signature_keyid = None

        if self.signature != "(none)":
            signature_match = Rpm.SIGNATURE_REGEX.fullmatch(self.signature)
            if signature_match:
                self.signature_format = signature_match.group(1)
                self.signature_date = signature_match.group(2)
                self.signature_keyid = signature_match.group(3)

    @property
    def dist(self):
        """Get the dist for this RPM e.g. ".fc30", ".el7", etc."""
        return "." + self.distro_code + self.releasever

    def oldinfo(self):
        """Get information string about an RPM."""
        cmd = ["rpm", "--query", "--package", "--info", self._path]
        completed_process = subprocess.run(cmd, capture_output=True,
                                           check=True, encoding="utf-8")
        return completed_process.stdout

    def __str__(self):
        """Return an informal string representation of this Rpm.

        This magic method controls the output of str(rpm) and
        print(rpm).
        """
        return "Path        : " + self._path + "\n" + self.info()

    def info(self):
        """Print the properties of this RPM."""
        print("type=rpm")
        print("path=" + self._path)
        print("filename=" + self.filename)
        print("name=" + self.name)
        print("version=" + self.version)
        print("release=" + self.release)
        print("distribution=" + self.distribution)
        print("specver=" + self.specver)
        print("distro_code=" + self.distro_code)
        print("releasever=" + self.releasever)
        print("arch=" + self.arch)
        print("is_source=" + str(self.is_source))
        print("signature=" + self.signature)
        print("signature_format=" + str(self.signature_format))
        print("signature_date=" + str(self.signature_date))
        print("signature_keyid=" + str(self.signature_keyid))


class Tarball(Artifact):
    """Represents a source tarball of some variety."""

    VALID_EXTENSIONS = (".tar",
                        ".tar.bz2",
                        ".tar.gz",
                        ".tar.xz",
                        ".tgz")

    @staticmethod
    def is_valid_filename(filename):
        """Return true if the filename corresponds to a tarball."""
        return filename.endswith(Tarball.VALID_EXTENSIONS)

    @staticmethod
    def is_valid_path(path):
        """Return true if path corresponds to a tarball."""
        return path.endswith(Tarball.VALID_EXTENSIONS) \
            and os.path.isfile(path)

    def __init__(self, path):
        """Contruct a Tarball."""
        logging.debug("Constructing tarball from '%s'", path)

        if Tarball.is_valid_path(path):
            super().__init__(path)
        else:
            raise ValueError(
                "The path {} does not correspond to "
                "a valid tarball file.".format(path))

    def __str__(self):
        """Return an informal string representation of this tarball.

        This magic method controls the output of str(rpm) and
        print(rpm).
        """
        return "Path        : " + self._path + "\n" + self.info()

    def info(self):
        """Print information about this tarball."""
        print("type=tarball")
        print("path=" + self._path)
        print("filename=" + self.filename)

    def oldinfo(self):
        """Get information string about a Tarball."""
        cmd = ["stat", self._path]
        completed_process = subprocess.run(cmd, capture_output=True,
                                           check=True, encoding="utf-8")
        return completed_process.stdout


class DetachedSignature(Artifact):
    """Represents a detached signature for another artifact."""

    VALID_EXTENSIONS = (".asc")

    @staticmethod
    def is_valid_filename(filename):
        """Return true if the filename corresponds to a detached signature."""
        return filename.endswith(DetachedSignature.VALID_EXTENSIONS)

    @staticmethod
    def is_valid_path(path):
        """Return true if path corresponds to a detached signature."""
        return path.endswith(DetachedSignature.VALID_EXTENSIONS) \
            and os.path.isfile(path)

    def __init__(self, path):
        """Contruct a DetachedSignature."""
        logging.debug("Constructing detached signature from '%s'", path)

        if DetachedSignature.is_valid_path(path):
            super().__init__(path)
        else:
            raise ValueError(
                "The path {} does not correspond to "
                "a valid detached signature file.".format(path))

    def __str__(self):
        """Return an informal string representation of this detached signature.

        This magic method controls the output of str(artifact) and
        print(artifact).
        """
        return "Path        : " + self._path + "\n" + self.info()

    def info(self):
        """Get information string about a detached signature."""
        cmd = ["stat", self._path]
        completed_process = subprocess.run(cmd, capture_output=True,
                                           check=True, encoding="utf-8")
        return completed_process.stdout
