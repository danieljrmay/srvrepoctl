"""Command line arguments configuration and parsing code."""

import argparse


def parse_args(clargs_list=None):
    """Return an argpare.Namespace representing the parsed clargs_list.

    Parameters: clargs_list: The command line arguments as a list,
    (default=None). If clargs_list=None then sys.argv is assumed.

    Returns:
    argparse.Namespace: Representation of the parsed clargs_list

    """
    return parser().parse_args(clargs_list)


def parser():
    """Return an argparse.ArgumentParser for srvrepoctl’s command line args."""
    clarg_parser = argparse.ArgumentParser(
        prog="srvrepoctl",
        description="Utility for managing software repositories "
        "hosted on the local system.",
        add_help=True,
        allow_abbrev=False,
        epilog="For more information see "
        "https://github.com/danieljrmay/srvrepoctl or the man page."
    )

    cmd_subparsers = clarg_parser.add_subparsers(
        title="Commands",
        description="The following commands are available.",
        dest="command")

    _configure_add_command(cmd_subparsers)
    _configure_clean_command(cmd_subparsers)
    _configure_create_command(cmd_subparsers)
    _configure_demote_command(cmd_subparsers)
    _configure_destroy_comand(cmd_subparsers)
    _configure_info_command(cmd_subparsers)
    _configure_fileinfo_command(cmd_subparsers)
    _configure_list_command(cmd_subparsers)
    _configure_promote_command(cmd_subparsers)
    _configure_pull_command(cmd_subparsers)
    _configure_push_command(cmd_subparsers)
    _configure_remove_command(cmd_subparsers)
    _configure_repoinfo_command(cmd_subparsers)
    _configure_repolist_command(cmd_subparsers)
    _configure_update_command(cmd_subparsers)
    _configure_verify_command(cmd_subparsers)

    return clarg_parser


def _configure_add_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "add",
        description="Add artifacts to a repository.",
        help="Add artifacts to a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_assume_yes_arg(subparser)
    _add_skip_update_arg(subparser)
    _add_gpg_homedir_arg(subparser)
    _add_file_path_arg(subparser)


def _configure_clean_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "clean",
        description="Clean a repository or repositories by "
        "removing outdated artifacts.",
        help="Remove outdated artifacts from a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_assume_yes_arg(subparser)
    _add_keep_arg(subparser)
    _add_skip_update_arg(subparser)


def _configure_create_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "create",
        description="Create a repository on the filesystem.",
        help="Create a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_assume_yes_arg(subparser)
    _add_gpg_homedir_arg(subparser)


def _configure_demote_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "demote",
        description="Demote an artifact from one repository to another.",
        help="Demote artifacts from a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_skip_update_arg(subparser)
    _add_gpg_homedir_arg(subparser)
    _add_artifact_filename_arg(subparser)


def _configure_destroy_comand(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "destroy",
        description="Erase a repository from the filesystem.",
        help="Erase a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_assume_yes_arg(subparser)


def _configure_fileinfo_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "fileinfo",
        description="Show information about a specified file "
        "(potential artifact).",
        help="Show information about a specified file (potential artifact)")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_gpg_homedir_arg(subparser)
    _add_file_path_arg(subparser)


def _configure_info_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "info",
        description="Show information about an artifact in a repository.",
        help="Show information about an artifact in a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_gpg_homedir_arg(subparser)
    _add_artifact_filename_arg(subparser)


def _configure_list_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "list",
        description="List a repository’s artifacts.",
        help="List a repository’s artifacts")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)


def _configure_promote_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "promote",
        description="Promote an artifact from one repository to another.",
        help="Promote artifacts from a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_skip_update_arg(subparser)
    _add_gpg_homedir_arg(subparser)
    _add_artifact_filename_arg(subparser)


def _configure_pull_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "pull",
        description="Pull artifacts from an upstream repository.",
        help="Pull artifacts from an upstream repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_skip_update_arg(subparser)
    _add_gpg_homedir_arg(subparser)


def _configure_push_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "push",
        description="Push artifacts to a downstream repository.",
        help="Push artifacts to a downstream repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_skip_update_arg(subparser)
    _add_gpg_homedir_arg(subparser)


def _configure_remove_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "remove",
        description="Remove artifacts from a repository.",
        help="Remove artifacts from a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_skip_update_arg(subparser)
    _add_assume_yes_arg(subparser)
    _add_artifact_filename_arg(subparser)


def _configure_repoinfo_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "repoinfo",
        description="Show infomation about a repository.",
        help="Show information about a repository")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_gpg_homedir_arg(subparser)


def _configure_repolist_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "repolist",
        description="Show list of repository IDs.",
        help="Show list of repository IDs")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)


def _configure_update_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "update",
        description="Update a repository’s metadata.",
        help="Update a repository’s metadata")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_gpg_homedir_arg(subparser)


def _configure_verify_command(cmd_subparsers):
    subparser = cmd_subparsers.add_parser(
        "verify",
        description="Verify a repository’s artifacts.",
        help="Verify a repository’s artifacts")
    _add_verbosity_group(subparser)
    _add_varible_args(subparser)
    _add_gpg_homedir_arg(subparser)


def _add_verbosity_group(subparser):
    """Add the mutually exclusive --debug and --verbose options."""
    messages_group = subparser.add_argument_group(
        "Messages",
        "Use these options to display more messages.")
    verbosity_group = messages_group.add_mutually_exclusive_group()
    verbosity_group.add_argument(
        "-d", "--debug",
        action="store_true",
        help="Show lots of very detailed debugging messages"
    )
    verbosity_group.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Show more messages (but not as many as --debug)"
    )


def _add_artifact_filename_arg(subparser):
    """Add the ARTIFACT_FILENAME... positional argument."""
    subparser.add_argument(
        "artifact_filename",
        nargs="+",
        help="Filename of the artifact to operate on",
        metavar="ARTIFACT_FILENAME"
    )


def _add_assume_yes_arg(subparser):
    """Add the -y, --assumeyes option."""
    subparser.add_argument(
        "-y", "--assumeyes",
        action="store_true",
        help="Automatically answer yes to all questions"
    )


def _add_deltas_arg(subparser):
    """Add the --deltas option."""
    subparser.add_argument(
        "--deltas",
        action="store_true",
        help="Force the creation of delta RPMs"
    )


def _add_file_path_arg(subparser):
    """Add the FILEPATH... positional argument."""
    subparser.add_argument(
        "file_path",
        nargs="+",
        help="File path to operate on",
        metavar="FILE_PATH"
    )


def _add_keep_arg(subparser):
    """Add the --keep option."""
    subparser.add_argument(
        "--keep", type=int,
        help="Number of artifacts to keep when cleaning a repository")




def _add_skip_update_arg(subparser):
    subparser.add_argument(
        "--skip-update",
        action="store_true",
        help="Skip update of repository metadata"
    )


# pylint: disable=W0511
def _add_varible_args(subparser):
    """Add the --arch, --basearch and --releasever options."""
    variables_group = subparser.add_argument_group(
        "Repository configuration variables",
        "Use these options to override the default values of the "
        "variables which are used in the *.srvrepoctl repository "
        "configuration files.")
    variables_group.add_argument(
        "--arch",
        # TODO: self pecify list of permissible values
        help="Override the default arch variable value"
    )
    variables_group.add_argument(
        "--basearch",
        # TODO: specify list of permissible values
        help="Override the basearch variable value"
    )
    variables_group.add_argument(
        "--releasever",
        type=int,
        help="Override the releasever variable value"
    )
    subparser.add_argument(
        "--repo",
        action="append",
        help="The id of the repository to operate on, "
        "can be used multiple times"
    )
    subparser.add_argument(
        "--config-dir",
        default="/etc/srvrepoctl.d",
        help="Specify the directory which contains the *.srvrepoctl "
        "repository configuration files"
    )


def _add_gpg_homedir_arg(subparser):
    """Add the ---gpg-homedir option."""
    subparser.add_argument(
        "--gpg-homedir",
        default=None,
        help="Specify the GPG home directory"
    )
