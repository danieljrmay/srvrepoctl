"""Variables which can be used in srvrepoctl configuration files."""

import dnf


class Variables():
    """Variables which can be used in *.srvrepoctl files."""

    VALID_ARCH_VALUES = ["aarch64", "alpha", "alphaev4", "alphaev45",
                         "alphaev5", "alphaev56", "alphaev6",
                         "alphaev67", "alphaev68", "alphaev7",
                         "alphapca56", "armv5tejl", "armv5tel",
                         "armv5tl", "armv6l", "armv7l", "armv8l",
                         "armv6hl", "armv7hl", "armv7hnl", "armv8hl",
                         "armv8hnl", "armv8hcnl", "i386", "athlon",
                         "geode", "i386", "i486", "i586", "i686",
                         "ia64", "mips", "mipsel", "mips64",
                         "mips64el", "noarch", "ppc", "ppc64",
                         "ppc64iseries", "ppc64p7", "ppc64pseries",
                         "ppc64le", "riscv32", "riscv64", "riscv128",
                         "s390", "s390x", "sh3", "sh4", "sh4a",
                         "sparc", "sparc64", "sparc64v", "sparcv8",
                         "sparcv9", "sparcv9v", "x86_64", "amd64",
                         "ia32e"]
    VALID_BASEARCH_VALUES = ["aarch64", "alpha", "arm", "armhfp",
                             "i386", "ia64", "mips", "mipsel",
                             "mips64", "mips64el", "noarch", "ppc",
                             "ppc64", "ppc64le", "riscv32", "riscv64",
                             "riscv128", "s390", "s390x", "sh3",
                             "sh4", "sparc", "x86_64"]
    VALID_RELEASEVER_MIN = 1
    VALID_RELEASEVER_MAX = 40

    def __init__(self, arch=None, basearch=None, releasever=None):
        """Construct a Variables object, using the Rpm if specified or
        current system's dnf substitutions as the initial values.
        """
        self._variables = dnf.dnf.Base().conf.substitutions

        if arch:
            self.override_arch(arch)

        if basearch:
            self.override_basearch(basearch)

        if releasever:
            self.override_releasever(releasever)

    @property
    def dictionary(self):
        """Get the variables as a dictionary."""
        return self._variables

    @property
    def arch(self):
        """Returns the $arch variable value."""
        return self._variables["arch"]

    @property
    def basearch(self):
        """Return the $basearch variable value."""
        return self._variables["basearch"]

    @property
    def releasever(self):
        """Return the $releasever variable value."""
        return self._variables["releasever"]

    def override_arch(self, arch):
        """Override the $arch variable value."""
        if arch not in Variables.VALID_ARCH_VALUES:
            raise ValueError(
                "The provided value of arch={} is not in the list of "
                "valid values: {}.".format(
                    arch, ", ".join(Variables.VALID_ARCH_VALUES)
                )
            )

        if arch != "noarch":
            self._variables["arch"] = arch

    def override_basearch(self, basearch):
        """Override the $basearch variable value."""
        if basearch not in Variables.VALID_BASEARCH_VALUES:
            raise ValueError(
                "The provided value of basearch={} is not in the list of "
                "valid values: {}.".format(
                    basearch, ", ".join(Variables.VALID_BASEARCH_VALUES)
                )
            )

        if basearch != "noarch":
            self._variables["basearch"] = basearch

    def override_releasever(self, releasever):
        """Override the $releasever variable value."""
        if Variables.VALID_RELEASEVER_MIN <= int(releasever) \
           <= Variables.VALID_RELEASEVER_MAX:
            self._variables["releasever"] = str(releasever)
        else:
            raise ValueError(
                "The provided value of releasever={} is not in the legal "
                "range of {} to {}.".format(
                    releasever,
                    Variables.VALID_RELEASEVER_MIN,
                    Variables.VALID_RELEASEVER_MAX
                )
            )
