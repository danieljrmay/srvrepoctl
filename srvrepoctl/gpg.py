"""GPG signing and verifying related code."""

import logging
import os
import gnupg

from srvrepoctl.artifacts import DetachedSignature

DEFAULT_RPM_GPG_KEY_DIRECTORY = "/etc/pki/rpm-gpg"

_gpg = None  # pylint: disable=invalid-name


def is_init():
    """Return true if _gpg has been initialized."""
    global _gpg  # pylint: disable=invalid-name,global-statement
    if _gpg is None:
        return False

    return True


def uninit():
    """Uninitalise _gpg."""
    logging.info("Uninitalising GPG home directory.")
    global _gpg  # pylint: disable=invalid-name,global-statement
    _gpg = None


def init(custom_homedir=None):
    """Initialize _gpg."""
    global _gpg  # pylint: disable=invalid-name,global-statement
    if gnupghome is None:
        logging.info("Initalising GPG to use it’s default homedir "
                     "this is typically '~/.gnupg', but could customised "
                     "by the GNUPGHOME environment variable.")
    else:
        logging.info("Initalising GPG to use homedir='%s'.")

    _gpg = gnupg.GPG(gnupghome=custom_homedir)


def gnupghome():
    """Get the value of gnugphome for the current gpg instance."""
    global _gpg  # pylint: disable=invalid-name,global-statement
    if _gpg is None:
        raise GpgUninitialisedError()

    return _gpg.gnupghome


def import_keys(path):
    """Import keys stored in a keyfile."""
    global _gpg  # pylint: disable=invalid-name,global-statement
    if _gpg is None:
        raise GpgUninitialisedError()

    if not os.path.isfile(path):
        logging.error("The key file path '%s' does not exist.", path)
        raise FileNotFoundError(path)

    logging.info("Importing keys from '%s'.", path)
    with open(path, "r") as key_file:
        key_data = key_file.read()

    return _gpg.import_keys(key_data)


def scan_keys(path):
    """Get keys from specified path."""
    global _gpg  # pylint: disable=invalid-name,global-statement
    if _gpg is None:
        raise GpgUninitialisedError()

    if not os.path.isfile(path):
        logging.error("The key file path '%s' does not exist.", path)
        raise FileNotFoundError(path)

    logging.info("Scanning keys from '%s'.", path)
    return _gpg.scan_keys(path)


def sign_file(path, keyid, passphrase=None):
    """Sign a file with a detacted signature."""
    global _gpg  # pylint: disable=invalid-name,global-statement
    if _gpg is None:
        raise GpgUninitialisedError()

    if not os.path.isfile(path):
        logging.error("Can not sign '%s' because it does "
                      "not exist on the filesystem.", path)
        raise FileNotFoundError(path)

    output_path = path + ".asc"
    logging.info("Creating detached signature file '%s'.", output_path)

    with open(path, "rb") as stream:
        _gpg.sign_file(
            stream,
            keyid=keyid,
            passphrase=passphrase,
            detach=True,
            output=output_path)

    return DetachedSignature(output_path)


def _get_keys(path):
    """Get list of keyids represented by key files."""
    global _gpg  # pylint: disable=invalid-name,global-statement
    if _gpg is None:
        raise GpgUninitialisedError()

    return _gpg.scan_keys(path)


def _file_contains_key(path, keyid):
    """True if path contains key with specified keyid."""
    for key in _get_keys(path):
        if key["keyid"].upper().endswith(keyid.upper()):
            return True

    return False


def _get_key_file_paths(directory=DEFAULT_RPM_GPG_KEY_DIRECTORY):
    """Get a list of potential key file paths from local filesystem."""
    paths = []
    for filename in os.listdir(directory):
        path = os.path.join(directory, filename)
        if (
                os.path.isfile(path) and
                not os.path.islink(path)):
            paths.append(path)

    return paths


def _find_key_file_path(keyid, directory=DEFAULT_RPM_GPG_KEY_DIRECTORY):
    """Find the path for for the specified keyid."""
    for path in _get_key_file_paths(directory):
        if _file_contains_key(path, keyid):
            return path

    return False


class GpgUninitialisedError(Exception):
    """Error caused when srvrepoctl.gpg has not been initialized."""

    def __init__(self):
        super().__init__("The srvrepoctl.gpg module "
                         "has not been initialized.")
