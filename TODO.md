# srvrepoctl TODOs #

* [x] Convert Repository to ABC, and implement SignedTarball & Rpm.
* [x] Fix failing tests.
* [x] Fix failing lints.
* [x] Correct naming of modules and packages (plurals in particular).
* [x] Fix "No test case for releasever=X".
* [x] Implement artifact abstract base class.
* [x] Change rpm(s) abstract method parameter to artifact(s).
* [x] Create artifact base class, for Rpm and Tarball.
* [x] Rename SignedTarballRepository as TarballRepository.
* [x] Fix repository base class implementation and testing.
* [x] Add tarball repository to testing configuration.
* [x] Make signing optional in TarballRepository.
* [x] Setup /tmp/srvrepoctl as test base directory.
* [x] Fix gpg home directory and implementation so test key can be
      imported.
* [x] Get push working for Tarball repository.
* [x] Check RPM key exists before scanning.
* [x] Clobber artifacts when adding by default.
* [x] Create detached signature as an artifact.
* [x] Add repolist command.
* [x] Rename `--repos-config-dir` option `--config-dir`.
* [x] Fix bash-completion script to add artifact paths and filenames.
* [x] Check `remove` command will work with new way `artifact_list`is generated.
* [x] Check bash-completion, man page etc are added to RPM.
* [x] Remove single - when double tabbing when not required in
      bash-completion.
* [x] Change Warnings to Infos where appropraite.
* [x] Change bulid of man pages to `build/man` rather than `man`.
* [x] Create all man page option includes.
* [x] Remove _deprecated function from gpg.py
* [x] Change push/pull commands to use `rsync -a` so we keep file metadata.
* [x] Clobber old man gzip files when re-running `make man`.
* [ ] Fix `add` and `push` commands which currently do not seem to be
      working.
* [ ] Handle creating `/srv/{rpm,src}` directories when initatlly
      creating reposiories. Ask for `sudo` permission if required to
      create a directory.
* [ ] Update Fedora RPM key which is attempted to be imported when
      running `make requirements`
* [ ] Finish implement info command.
* [ ] Consider having an UnkownArtifact object?
* [ ] Go through code base emphasising difference between a file and
      and artifact.
* [ ] Create man page for each command.
* [ ] Implement commands on TarballRepository class.
* [ ] Improve info() method for artifacts.
* [ ] Update `exit_status.inc.doc` documentation to match programme.
* [ ] Add verify command (which checks all signatures of repository).
* [ ] Create web-page based manual based on adoc files.
* [ ] Do not instantiate gpg in `Srvrepoctl.__init__()` when it is not required.
* [ ] Match repolist command to dnf, but add a quiet version which
      gives only ids for use in Bash-completion functions.
* [ ] Consider refactor to make more pure-functional, particularly gpg module.
* [ ] Remove used options from bash-completion suggestions.
* [ ] Add flatpaks.
* [ ] Add fish shell completions.
* [ ] Add `--demote-to repoid` and `--promote-to repoid` options to
      the `demote` and `promote` commands respectivley.
