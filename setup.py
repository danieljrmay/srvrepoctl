"""setuptools configuration for the srvrepoctl package.

See: https://packaging.python.org/tutorials/packaging-projects
"""

import setuptools

AUTHOR = "Daniel J. R. May"
AUTHOR_EMAIL = "daniel.may@danieljrmay.com"
LICENSE = "GNU General Public License v3 (GPLv3)"

with open("./python-srvrepoctl.spec", "r") as fh:
    for line in fh:
        if line.startswith("Version:"):
            VERSION = line[8:].strip()
            break

with open("README.md", "r") as fh:
    LONG_DESCRIPTION = fh.read()

setuptools.setup(
    name="srvrepoctl",
    version=VERSION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    maintainer=AUTHOR,
    maintainer_email=AUTHOR_EMAIL,
    description=("A command line utility to help with the administration "
                 "of software repositories which you serve."),
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/danieljrmay/srvrepoctl",
    license=LICENSE,
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: " + LICENSE,
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha",
    ],
    entry_points={
        "console_scripts": ["srvrepoctl = srvrepoctl.__main__:main"],
    },
    test_suite="srvrepoctl.test",
    include_package_data=False,
)
