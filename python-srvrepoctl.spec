%global srcname srvrepoctl

Name:           python-%{srcname}
Version:        0.1
Release:        1%{?dist}
Summary:        Tool for curating the RPM repositories you host

License:        GPLv3
URL:            https://gitlab.com/danieljrmay/%{srcname}
Source0:        %{url}/-/archive/%{version}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch


%description
Tool for curating the RPM repositories you host.


%package -n python3-%{srcname}
Summary:       Python 3 build of the srvrepoctl module
BuildRequires: python3-devel python3-distro python3-dnf python3-gnupg python3-rpm python3-tox
Requires:      createrepo_c dnf-utils python3-distro python3-dnf python3-gnupg rsync
%{?python_provide:%python_provide python3-%{srcname}}

%description -n python3-%{srcname}
The Python 3 module which provides the core functionality for the
srvrepoctl tool.


%package -n %{srcname}
Summary:  %{summary}
Requires: bash-completion python3-%{srcname} = %{version}-%{release}
BuildRequires: make rubygem-asciidoctor

%description -n %{srcname}
Tool for curating the RPM repositories you host.


%prep
%autosetup -n %{srcname}-%{version}


%build
%py3_build
%make_build


%install
%py3_install
%make_install


%check
%{__python3} setup.py test


%files -n python3-%{srcname}
%doc README.md
%license LICENSE
%{python3_sitelib}/*


%files -n %{srcname}
%doc README.md
%license LICENSE
%{_bindir}/%{srcname}
%{_mandir}/man1/%{srcname}.1.gz
%{_datadir}/bash-completion/completions/%{srcname}


%changelog
* Thu Aug 15 2019 Daniel J. R. May <daniel.may@danieljrmay.com> - 0.1-1
- Initial release.

