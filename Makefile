#!/usr/bin/make -f
#
# rpmrepoctl GNU Makefile
#
# See: https://www.gnu.org/software/make/manual
#
# Copyright (c) 2018 Daniel J. R. May
#

# Makefile command variables
DNF=/usr/bin/dnf
GIT=/usr/bin/git
GZIP=/usr/bin/gzip
INSTALL=/usr/bin/install
INSTALL_DATA=$(INSTALL) --mode=644 -D
INSTALL_DIR=$(INSTALL) --directory
INSTALL_PROGRAM=$(INSTALL) --mode=755 -D
MOCK=/usr/bin/mock
PYINILINT=/usr/bin/pyinilint
PYLINT=/usr/bin/pylint
PYTHON=/usr/bin/python3
PYTHON_SETUP=/usr/bin/python3 setup.py
RPM=/usr/bin/rpm
RPM_IMPORT=$(RPM) --import
RPMLINT=/usr/bin/rpmlint
SED=/usr/bin/sed
SHELL=/bin/sh
SHELLCHECK=/usr/bin/shellcheck
SHELLCHECK_X=$(SHELLCHECK) -x
XZ=/usr/bin/xz
YAMLLINT=/usr/bin/yamllint

# Standard Makefile installation directories.
#
# The following are the standard GNU/RPM values which are defined in
# /usr/lib/rpm/macros. See
# http://www.gnu.org/software/make/manual/html_node/Directory-Variables.html
# for more information.
prefix=/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
datadir=$(prefix)/share
includedir=$(prefix)/include
infodir=$(prefix)/info
libdir=$(exec_prefix)/lib
libexecdir=$(exec_prefix)/libexec
localstatedir=$(prefix)/var
mandir=$(prefix)/man
rpmconfigdir=$(libdir)/rpm
sbindir=$(exec_prefix)/sbin
sharedstatedir=$(prefix)/com
sysconfdir=$(prefix)/etc

# Fedora installation directory overrides.
#
# We override some of the previous GNU/RPM default values with those
# values suiteable for a Fedora/RedHat/CentOS linux system, as defined
# in /usr/lib/rpm/redhat/macros.
#
# This section can be put into a separate file and included here if we
# want to create a more multi-platform Makefile system.
sysconfdir=/etc
defaultdocdir=/usr/share/doc
infodir=/usr/share/info
localstatedir=/var
mandir=/usr/share/man
sharedstatedir=$(localstatedir)/lib

# Makefile parameter variables
name:=srvrepoctl
version=$(shell awk '/Version:/ { print $$2 }' python-$(name).spec)
iso8601date:=$(shell date --iso-8601)
dist_name:=$(name)-$(version)
dist_tree-ish:=$(version)
tarball:=$(dist_name).tar.gz
fedora_releasever:=$(shell python3 -c 'import dnf; db = dnf.dnf.Base(); print(db.conf.substitutions["releasever"])')
fedora_dist:=.fc$(fedora_releasever)
fedora_rpm_stem:=$(shell rpm --define='dist $(fedora_dist)' --specfile python-$(name).spec)
fedora_rpm:=$(fedora_rpm_stem).rpm
fedora_srpm:=python-$(dist_name)-1$(fedora_dist).src.rpm
fedora_mock_root:=fedora-$(fedora_releasever)-x86_64-kada-media
mock_resultdir=.
testdestdir:=testdestdir
git_hooks_tgt_files:=$(patsubst hooks/%,.git/hooks/%,$(wildcard hooks/*))
requirements:=bash-completion createrepo_c dnf-utils git mock pylint python3-distro python3-dnf python3-gnupg rpm-build rpmlint rsync rubygem-asciidoctor ShellCheck yamllint
rpm_key_urls:=https://getfedora.org/static/429476B4.txt
man_adoc_srcs:=$(wildcard doc/man/*.adoc)

.PHONY: all
all: bash-completion man pybuild 
	$(info all:)

.PHONY: bash-completion
bash-completion: bash-completion/$(name)

bash-completion/$(name): bash-completion/$(name).bash
	$(SED) -e "s/%ISO8601DATE%/$(iso8601date)/g" -e "s/%VERSION%/$(version)/g" $< > $@

.PHONY: man
man: uncompressedman
	$(GZIP) --best --force --recursive build/man/*

.PHONY: uncompressedman
uncompressedman: $(man_adoc_srcs)
	mkdir -p build/man
	asciidoctor --backend manpage --destination-dir build/man $(man_adoc_srcs)

.PHONY: pybuild
pybuild:
	$(PYTHON_SETUP) build

.PHONY: lint
lint:
	$(info lint:)
	$(PYLINT) $(name) setup.py
	$(SHELLCHECK_X) bash-completion/*.bash
	$(RPMLINT) *.spec
	$(YAMLLINT) .gitlab-ci.yml

.PHONY: test
test:
	$(info test:)
	$(PYTHON_SETUP) test

.PHONY: pyinstall
pyinstall:
	$(PYTHON_SETUP) install

testdestdir:
	mkdir -p $(testdestdir)
	$(eval DESTDIR:=$(testdestdir))

.PHONY: testinstall
testinstall: | testdestdir install installcheck

.PHONY: install
install:
	$(info install:)
	$(INSTALL_DATA) bash-completion/$(name).bash $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	$(INSTALL_DATA) --target-directory=$(DESTDIR)$(mandir)/man1 build/man/*.1.gz

.PHONY: installcheck
installcheck:
	$(info installcheck:)
	test -r $(DESTDIR)$(datadir)/bash-completion/completions/$(name)
	test -r $(DESTDIR)$(mandir)/man1/$(name).1.gz

.PHONY: installgithooks
installgithooks: $(git_hooks_tgt_files)
	$(info All git_hook have been installed in .git/hooks directory.)

$(git_hooks_tgt_files): git_hooks/*
	cp $< $@

# Generate a test distribution tarball from the local files.
.PHONY: testdist
testdist:
	$(info testdist:)
	mkdir -p $(dist_name)
	cp -r --target-directory $(dist_name) \
		bash-completion doc hooks LICENSE Makefile pylintrc python-srvrepoctl.spec \
		README.md setup.py srvrepoctl TODO.md
	tar --create --gzip --file $(tarball) $(dist_name)
	rm -rf $(dist_name)

# Generate a distribution tarball from a repository tag, not the local files.
.PHONY: dist
dist:
	$(GIT) archive --format=tar.gz --prefix=$(dist_name)/ $(dist_tree-ish) > $(tarball)

.PHONY: srpm
srpm: $(tarball)
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --buildsrpm \
		--spec python-$(name).spec --sources $(tarball)

.PHONY: rpm
rpm: srpm
	$(MOCK) --root=$(fedora_mock_root) --resultdir=$(mock_resultdir) --rebuild $(fedora_srpm)

.PHONY: clean
clean:
	$(info clean:)
	find . -name '*.pyc' -delete
	find . -name '__pycache__' -delete
	rm -f *.rpm *.tar.gz 
	rm -rf build dist 

.PHONY: distclean
distclean: clean
	$(info distclean:)
	find . -name '*~' -delete
	rm -f *.log doc/man/*.1 bash-completion/$(name)
	rm -rf dist testdestdir $(name).egg-info

.PHONY: requirements
requirements:
	sudo $(DNF) copr --assumeyes enable danieljrmay/pyinilint
	sudo $(DNF) install --assumeyes $(requirements)
	sudo $(RPM_IMPORT) $(rpm_key_urls)

.PHONY: help
help:
	$(info help:)
	$(info Usage: make TARGET [VAR1=VALUE VAR2=VALUE])
	$(info )
	$(info Targets:)
	$(info   all             The default target, redirects to build.)
	$(info   lint            Lint the source files.)
	$(info   testinstall     Perform a test installation.)
	$(info   install         Install.)
	$(info   installcheck    Post-installation check of all installed files.)
	$(info   installgithooks Install the git hooks in the local repository.)
	$(info   dist		 Create a distribution tarball.)
	$(info   srpm		 Create a SRPM.)
	$(info   rpm		 Create a RPM.)
	$(info   clean           Clean up all generated binary files.)
	$(info   distclean       Clean up all generated files.)
	$(info   help            Display this help message.)
	$(info   printvars       Print variable values (useful for debugging).)
	$(info   printmakevars   Print the Make variable values (useful for debugging).)
	$(info )

.PHONY: printvars
printvars:
	$(info printvars:)
	$(info DNF_INSTALL=$(DNF_INSTALL))
	$(info GIT=$(GIT))
	$(info INSTALL=$(INSTALL))
	$(info INSTALL_DATA=$(INSTALL_DATA))
	$(info INSTALL_DIR=$(INSTALL_DIR))
	$(info INSTALL_PROGRAM=$(INSTALL_PROGRAM))
	$(info MOCK=$(MOCK))
	$(info PYINILINT=$(PYINILINT))
	$(info RPM=$(RPM))
	$(info RPM_IMPORT=$(RPM_IMPORT))
	$(info RPMLINT=$(RPMLINT))
	$(info SHELL=$(SHELL))
	$(info SHELLCHECK=$(SHELLCHECK))
	$(info SHELLCHECK_X=$(SHELLCHECK_X))
	$(info XZ=$(XZ))
	$(info prefix=$(prefix))
	$(info exec_prefix=$(exec_prefix))
	$(info bindir=$(bindir))
	$(info datadir=$(datadir))
	$(info includedir=$(includedir))
	$(info infodir=$(infodir))
	$(info libdir=$(libdir))
	$(info libexecdir=$(libexecdir))
	$(info localstatedir=$(localstatedir))
	$(info mandir=$(mandir))
	$(info rpmconfigdir=$(rpmconfigdir))
	$(info sbindir=$(sbindir))
	$(info sharedstatedir=$(sharedstatedir))
	$(info sysconfdir=$(sysconfdir))

.PHONY: printmakevars
printmakevars:
	$(info printmakevars:)
	$(info $(.VARIABLES))
